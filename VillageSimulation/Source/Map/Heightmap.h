#ifndef HEIGHTMAP_H
#define HEIGHTMAP_H

#include <string>
#include <numeric>
#include <random>
#include <vector>
#include <algorithm>    // std::sort
#include <functional>   // std::greater
#include <SFML/Graphics.hpp>
#include <iostream>
//#include "Pathfinder.h"

//struct gridData
//{
//	int heightVal = 0;
//};

enum direction{

	north = 0,
	east = 1,
	south = 2,
	west = 3
};

struct edgeInfo
{
	std::pair<int, int> firstEdge; //Side, Edge
	std::pair<int, int> secondEdge;
};

struct tileData
{
	float nwHeightVal = 0;
	float neHeightVal = 0;
	float swHeightVal = 0;
	float seHeightVal = 0;
	float heightAverageVal = 0.0f;
	bool hasRoad = false;
	bool hasBuilding = false;
	bool isWater = false;
};

struct vertexVals
{
	int redVal = 0;
	int greenVal = 0;
	int blueVal = 0;
};


class Heightmap 
{
	//Declare constants
	//int mapWidth = 1024 - 1, mapHeight = 1024 - 1, tileSize = 2, numberPeople = 1; //map width/height converted to tiles
	const int tileSize = 2, numberPeople = 1; //map width/height converted to tiles
	int onClosedList = 10;
	const int notfinished = 0, notStarted = 0;// path-related constants
	const int found = 1, nonexistent = 2;
	const int walkable = 0, unwalkable = 1;// walkability array constants


	public:
		Heightmap();
		Heightmap(float scaleVal);
		~Heightmap();

		bool loadHeightMap(const std::string & fileName);
		sf::Image& createHeightMapImage();
		std::vector<int> findPath(int startX, int startY, int targetX, int targetY);

		bool getMapChanged();
		bool getWaterInterestChanged();
		bool getRoadInterestChanged();
		bool getEdgeConnectChanged();
		bool getUnwalkableInterestChanged();
		std::vector<sf::Vector2i>* getRoadAccessPoints();

		std::vector<tileData>* getMapData();
		std::vector<int>& getWaterInterestData();
		std::vector<int>& getRoadInterestData();
		std::vector<int>& getEdgeConnectData();
		std::vector<int>& getUnwalkableInterestData();
		void startFillArea();
		void fillArea(int startX, int startY, int fillIndex);

		bool checkVillagePlacement(sf::Vector2i tempPos, int radius);


		void generateRoadConnections();


		vertexVals getVertexData(int location);
		void setRoadInfo(std::vector<int>& tempPathInfo);
		void setBuildingInfo(std::vector<int>& tempPathInfo);
		void setSeed(double newSeed);
		double getSeed();
		void clearRoadInfo();
		void setScaleVal(float scaleVal);

		void displayRoadInterestTime();

		int getRows();
		int getCols();
		std::vector<sf::Vector2i> getEdgeWalkability(); //returns as vector
		std::vector<std::vector<std::vector<sf::Vector2i>>>* getEdgeVectors();

		void testEdges();



		//tileData

		sf::Image& generateWaterInterestMap(); //Used in pathfinding to deter from following to close to coast/water line, also to appeal to more coastal/water features
		sf::Image& generateRoadInterestMap();
		sf::Image& generateEdgeConnectMap();
		sf::Image& generateUnwalkableInterestMap();
		void generateEdgeConnections();


		//pathfinder

		void InitializePathfinder(const int width, const int height);
		void EndPathfinder(void);
		int FindPath(int pathfinderID, int startingX, int startingY, int targetX, int targetY);
		void ReadPath(int pathfinderID, int currentX, int currentY, int pixelsPerFrame);
		int ReadPathX(int pathfinderID, int pathLocation);
		int ReadPathY(int pathfinderID, int pathLocation);
		int calcSteepness(int parentXval, int parentYval, int a, int b);
		int calcWaterInterestCost(int targetX, int targetY);
		int calcRoadInterestCost(int targetX, int targetY);
		int calcUnwalkableInterestCost(int targetX, int targetY);

		inline int indexTranslate(int xLoc, int yLoc);


	private:

		float heightScale;

		int mapWidth;
		int mapHeight;

		std::vector<tileData> mapData; //2d Dynamic Array basicly
		std::vector<vertexVals> vertexData; //Vertex Container

		std::vector<std::vector<int>> fillWalkability; //holds walkable areas
		std::vector<int> edgeVec; //holds temp edgevalues

		
		std::vector<std::vector<std::vector<sf::Vector2i>>> edgeConnections;
		//std::vector<std::pair<std::vector<sf::Vector2i>, int>> edgeConnections;
		std::vector<sf::Vector2i> roadAccessPoints; //Stored x,y
		std::vector<std::vector<edgeInfo>> edgeConnectionInfo; //Holds info on connected edges

		
		int rows = 0;
		int cols = 0;
		sf::Image mapSprite;
		sf::Uint8* standardColourArray;

		sf::Image waterInterestSprite;
		std::vector<int> waterInterestData;
		sf::Uint8* waterInterestColourArray;

		sf::Image unwalkableInterestSprite;
		std::vector<int> unwalkableInterestData;
		sf::Uint8* unwalkableInterestColourArray;

		sf::Image roadInterestSprite;
		std::vector<int> roadInterestData;
		sf::Uint8* roadInterestColourArray;

		sf::Image edgeConnectSprite;
		std::vector<int> edgeConnectData;
		sf::Uint8* edgeConnectColourArray;

		bool mapChanged = true;
		bool waterInterestChanged = true;
		bool roadInterestChanged = true;
		bool edgeConnectChanged = true;
		bool unwalkableInterestChanged = true;

		int xLoc[4]; 
		int yLoc[4];

		std::mt19937 mtRandomGenerator;
		double heightmapSeed;

		double roadInterestTime;

		//PathFinder

		std::vector<char> walkability;
		std::vector<char> roadWalkability;
		std::vector<int> openList;				//1 dimensional array holding ID# of open list items
		std::vector<int> whichList;				//2 dimensional array used to record 
		std::vector<int> openX;					//1d array stores the x location of an item on the open list
		std::vector<int> openY;					//1d array stores the y location of an item on the open list
		std::vector<int> parentX;				//2d array to store parent of each cell (x)
		std::vector<int> parentY;				//2d array to store parent of each cell (y)
		std::vector<int> Fcost;					//1d array to store F cost of a cell on the open list
		std::vector<int> Gcost;					//2d array to store G cost for each cell.
		std::vector<int> Hcost;					//1d array to store H cost of a cell on the open list
		std::vector<int> pathLength;			//stores length of the found path for critter
		std::vector<int> pathLocation;			//stores current position along the chosen path for critter	
		std::vector<std::vector<int>> pathBank;
		std::vector<int> pathStatus;
		std::vector<int> xPath;
		std::vector<int> yPath;


};

#endif