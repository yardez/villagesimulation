#ifndef ASTAR_H
#define ASTAR_H

#include <stdlib.h>   // For _MAX_PATH definition
#include <stdio.h>
#include <malloc.h>
#include <vector>
#include <algorithm>
#include "Heightmap.h"

//Modifed to use data from 2D heightmap data which contains 3D height values. Also uses various costs associated with the mapdata to create more realistic paths.
/*
;===================================================================
;A* Pathfinder (Version 1.71a) by Patrick Lester. Used by permission.
;===================================================================
;Last updated 06/16/03 -- Visual C++ version
*/


class Pathfinder
{
	//Declare constants
	int mapWidth = 1024 - 1, mapHeight = 1024 - 1, tileSize = 2, numberPeople = 1; //map width/height converted to tiles
	int onClosedList = 10;
	const int notfinished = 0, notStarted = 0;// path-related constants
	const int found = 1, nonexistent = 2;
	const int walkable = 0, unwalkable = 1;// walkability array constants


public:
	Pathfinder();
	~Pathfinder();



	void InitializePathfinder(const int width, const int height);
	void EndPathfinder(void);
	int FindPath(int pathfinderID, int startingX, int startingY, int targetX, int targetY, Heightmap* heightmapPTR);
	void ReadPath(int pathfinderID, int currentX, int currentY, int pixelsPerFrame);
	int ReadPathX(int pathfinderID, int pathLocation);
	int ReadPathY(int pathfinderID, int pathLocation);
	int calcSteepness(int parentXval, int parentYval, int a, int b, std::vector<tileData>& mapDataRef);
	int calcWaterInterestCost(int targetX, int targetY, std::vector<int>& waterDataRef);
	int calcRoadInterestCost(int targetX, int targetY, std::vector<int>& roadDataRef);
	int calcUnwalkableInterestCost(int targetX, int targetY, std::vector<int>& unwalkableDataRef);

	std::vector<char> walkability;
	std::vector<char> roadWalkability;
	std::vector<int> openList;				//1 dimensional array holding ID# of open list items
	std::vector<int> whichList;				//2 dimensional array used to record 
	std::vector<int> openX;					//1d array stores the x location of an item on the open list
	std::vector<int> openY;					//1d array stores the y location of an item on the open list
	std::vector<int> parentX;				//2d array to store parent of each cell (x)
	std::vector<int> parentY;				//2d array to store parent of each cell (y)
	std::vector<int> Fcost;					//1d array to store F cost of a cell on the open list
	std::vector<int> Gcost;					//2d array to store G cost for each cell.
	std::vector<int> Hcost;					//1d array to store H cost of a cell on the open list
	std::vector<int> pathLength;			//stores length of the found path for critter
	std::vector<int> pathLocation;			//stores current position along the chosen path for critter	
	std::vector<std::vector<int>> pathBank;
	std::vector<int> pathStatus;
	std::vector<int> xPath;
	std::vector<int> yPath;

	inline int Pathfinder::indexTranslate(int xLoc, int yLoc);


private:

	


	


};

#endif