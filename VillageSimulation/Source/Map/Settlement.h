#ifndef SETTLEMENT_H
#define SETTLEMENT_H

#include <random>
#include <cstdint>
#include <vector>
#include <algorithm>
#include <string>
#include <iostream>
#include "../RoadNetwork/RoadNetwork.h"

class Settlement
{
public:
	Settlement();
	Settlement(double seed, int x, int y, int size = 25, float jitter = 10, int sections = 2);
	~Settlement();

	bool generateMainRoad(int x, int y, int size = 25, float jitter = 10, int sections = 2);
	bool generateSecondaryRoad(int id, int x, int y, int size = 25, float jitter = 10, int sections = 2);
	bool generateTertiaryRoad(int x, int y, int size = 25, float jitter = 10, int sections = 2);

	void setSettlementSeed(double newSeed);
	double getSettlementSeed();

	bool generateBuildingSeeds();

	std::vector<sf::Vector2i>& getMainStreetLocs();
	std::vector<sf::Vector2i>& getSecondaryStreetLocs(int id);

	RoadNetwork& getRoadNetwork();

private:
	
	std::mt19937 mtRandomGenerator;
	//std::mt19937 mtRandomGenerator2;
	double settlementSeed;
	//coordinate rotateCood(coordinate tempCoord, float angle);
	sf::Vector2i rotateCoodPoint(int offsetX, int offsetY, float angle, const sf::Vector2i point);

	std::vector<sf::Vector2i> mainStreetLocs;
	std::vector<std::vector<sf::Vector2i>> secondaryStreetLocs;
	std::vector<sf::Vector2i> buildingSeedLocs;



	
	//std::uniform_real_distribution<double> numberDistribution;
};

#endif