#include "Heightmap.h"
//#include "aStarLibrary.h"

Heightmap::Heightmap()
{
	//InitializePathfinder();
	heightScale = 1;
	
};

Heightmap::Heightmap(float scaleVal)
{
	//InitializePathfinder();
	heightScale = scaleVal;
	roadInterestTime = 0.0f;
};

Heightmap::~Heightmap()
{
	EndPathfinder();
};

bool Heightmap::loadHeightMap(const std::string & fileName)
{
	sf::Clock clock; // starts the clock
	sf::Image heightMapImage;
	if (heightMapImage.loadFromFile(fileName))
	{
		const sf::Vector2u tempVec = heightMapImage.getSize();
		rows = tempVec.x;
		cols = tempVec.y;

		InitializePathfinder(cols, rows);

		int arrayLength = ((cols - 1) * (rows - 1));
		roadInterestColourArray = new sf::Uint8[arrayLength * 4]();
		//// allocation
		//mapData = new gridData*[rows];
		//for (int i = 0; i < rows; i++)
		//	mapData[i] = new gridData[cols];


		//gridData tempData;
		//sf::Color tempColour;
		//// initialization
		//for (int j = 0; j < cols; j++)
		//	for (int i = 0; i < rows; i++)
		//	{
		//		 tempColour = heightMapImage.getPixel(i, j);
		//		 tempData.heightVal = tempColour.r; // Gets height value from red channel
		//		 mapData.push_back(tempData);
		//	}

		tileData tempData;
		//sf::Color tempColour;
		// initialization
		//for (int j = 0; j < cols; j++)
		//	for (int i = 0; i < rows; i++)
		//	{
		//	tempColour = heightMapImage.getPixel(i, j);
		//	tempData.heightVal = tempColour.r; // Gets height value from red channel
		//	mapData.push_back(tempData);
		//	}

		for (int j = 0; j < rows; j++)
		{
			for (int i = 0; i < cols; i++)
			{
				vertexVals tempVertVals;
				tempVertVals.redVal = heightMapImage.getPixel(i, j).r;
				tempVertVals.greenVal = heightMapImage.getPixel(i, j).g;
				tempVertVals.blueVal = heightMapImage.getPixel(i, j).b;


				vertexData.push_back(tempVertVals);
			}
		}



		std::vector<float> sortArray;

		for (int j = 0; j < rows-1; j++)
		{
			for (int i = 0; i < cols-1; i++)
			{
				tempData.isWater = false;
				int waterVal = 10;
				int waterDepth = 0;

				tempData.nwHeightVal = (i + 0 + ((j + 0)*rows));	  //Stores location of vertex
				tempData.neHeightVal = (i + 1 + ((j + 0)*rows));	  //Stores location of vertex
				tempData.swHeightVal = (i + 0 + ((j + 1)*rows));	  //Stores location of vertex
				tempData.seHeightVal = (i + 1 + ((j + 1)*rows));	  //Stores location of vertex

				sortArray.push_back(getVertexData(tempData.nwHeightVal).redVal);	//North West vertex.
				sortArray.push_back(getVertexData(tempData.neHeightVal).redVal);	//North West vertex.
				sortArray.push_back(getVertexData(tempData.swHeightVal).redVal);	//South East vertex.
				sortArray.push_back(getVertexData(tempData.seHeightVal).redVal);	//South East vertex.

				std::sort(sortArray.begin(), sortArray.end());

				tempData.heightAverageVal = std::accumulate(sortArray.begin(), sortArray.end(), 0.0) / sortArray.size();
				if (tempData.heightAverageVal > 5.0f)
				{
					//std::cout << "Oi";
				}

				const int rockHeight = 100;
				const int tooSteep = 30;


				if (sortArray[3] - sortArray[0] > tooSteep)
				{
					//walkability[i][j] = 1;
					walkability[indexTranslate(i, j)] = 1;
				}
				else if (tempData.heightAverageVal  >= rockHeight)
				{
					//walkability[i][j] = 1;
					walkability[indexTranslate(i, j)] = 1;
				}
				//Check for water
				else if ((getVertexData(tempData.nwHeightVal).blueVal >= waterVal
					|| getVertexData(tempData.neHeightVal).blueVal >= waterVal
					|| getVertexData(tempData.swHeightVal).blueVal >= waterVal
					|| getVertexData(tempData.seHeightVal).blueVal >= waterVal) || 
					(  getVertexData(tempData.nwHeightVal).redVal <= waterDepth
					|| getVertexData(tempData.neHeightVal).redVal <= waterDepth
					|| getVertexData(tempData.swHeightVal).redVal <= waterDepth
					|| getVertexData(tempData.seHeightVal).redVal <= waterDepth))
				{
					tempData.isWater = true;
					//walkability[i][j] = 1;

					walkability[indexTranslate(i, j)] = 1;
				}
					


				mapData.push_back(tempData);
				//i++;

				sortArray.clear();
			}
			//j++;
		}
		//createHeightMapImage();
		waterInterestData.resize(mapData.size());//Make same size as mapData
		roadInterestData.resize(mapData.size());//Make same size as mapData
		unwalkableInterestData.resize(mapData.size());//Make same size as mapData
		std::cout << "Time Taken To Load and create map data : " << clock.getElapsedTime().asSeconds() << std::endl;
		return true;
	}
	return false;

	
};

void Heightmap::setScaleVal(float scaleVal)
{

	heightScale = scaleVal;
}


sf::Image& Heightmap::createHeightMapImage()
{
	//std::vector<sf::Uint8> colorArray;


	//delete standardColourArray;



	//std::vector<int> sortArray;
	//sf::Color green = sf::Color::Green;
	//const sf::Uint8 grass = 
	//sf::Uint8 grass;

	int arrayLength = ((cols-1) * (rows-1));
	standardColourArray = new sf::Uint8[arrayLength * 4];
	std::vector<int> sortArray(4,0);


	int pos = 0;
	tileData tempData;
	for (int j = 0; j < rows-1; j++)
	{
		for (int i = 0; i < cols-1; i++)
		{
			tempData = mapData[(i)+((j)* (rows-1))];
			const int rockHeight = 100;
			const int tooSteep = 30;

			if (!tempData.isWater)
			{

				sortArray[0] = getVertexData(tempData.neHeightVal).redVal;
				sortArray[1] = getVertexData(tempData.nwHeightVal).redVal;
				sortArray[2] = getVertexData(tempData.seHeightVal).redVal;
				sortArray[3] = getVertexData(tempData.swHeightVal).redVal;
				std::sort(sortArray.begin(), sortArray.end());

				float average = tempData.heightAverageVal;

				if ((sortArray[3] - sortArray[0]) > tooSteep)
				{

					standardColourArray[(pos * 4) + 0] = 25 + average;
					standardColourArray[(pos * 4) + 1] = 25 + average;
					standardColourArray[(pos * 4) + 2] = 25 + average;
					standardColourArray[(pos * 4) + 3] = 255;

				}
				else if (tempData.heightAverageVal >= rockHeight)
				{

					int colourMult = 1;
					colourMult = average - rockHeight;

					standardColourArray[(pos * 4) + 0] = 50 + colourMult;
					standardColourArray[(pos * 4) + 1] = 50 + colourMult;
					standardColourArray[(pos * 4) + 2] = 50 + colourMult;
					standardColourArray[(pos * 4) + 3] = 255;
				}
				else
				{
					standardColourArray[(pos * 4) + 0] = 0;
					standardColourArray[(pos * 4) + 1] = 25 + average;
					standardColourArray[(pos * 4) + 2] = 0;
					standardColourArray[(pos * 4) + 3] = 255;
				}

				if (tempData.hasRoad)
				{
					//Brown
					standardColourArray[(pos * 4) + 0] = 139;
					standardColourArray[(pos * 4) + 1] = 69;
					standardColourArray[(pos * 4) + 2] = 19;
					standardColourArray[(pos * 4) + 3] = 255;
				}
				if (tempData.hasBuilding)
				{
					//Brown
					standardColourArray[(pos * 4) + 0] = 33;
					standardColourArray[(pos * 4) + 1] = 33;
					standardColourArray[(pos * 4) + 2] = 33;
					standardColourArray[(pos * 4) + 3] = 255;
				}
			}
			else
			{
				//blue
				standardColourArray[(pos * 4) + 0] = 19;
				standardColourArray[(pos * 4) + 1] = 108;
				standardColourArray[(pos * 4) + 2] = 255;
				standardColourArray[(pos * 4) + 3] = 255;

			}
			//sortArray.clear();
			pos++;
		}
	}

	//if (mapSprite == NULL)
	/*int pos = 0;
	for (int j = 0; j < rows; j++)
	{
		for (int i = 0; i < cols; i++)
		{

			sortArray.push_back(mapData[(i + 0) + ((j + 0) * (rows))].heightVal);
			sortArray.push_back(mapData[(i + 1) + ((j + 0) * (rows))].heightVal);
			sortArray.push_back(mapData[(i + 0) + ((j + 1) * (rows))].heightVal);
			sortArray.push_back(mapData[(i + 1) + ((j + 1) * (rows))].heightVal);
			std::sort(sortArray.begin(), sortArray.end());

			//float average = std::accumulate(sortArray.begin(), sortArray.end(), 0.0) / sortArray.size();
			//
			//colorArray[(pos * 4) + 0] = average;
			//colorArray[(pos * 4) + 1] = average;
			//colorArray[(pos * 4) + 2] = average;
			//colorArray[(pos * 4) + 3] = 255;


			const int rockHeight = 100;
			const int tooSteep = 30;

			float average = std::accumulate(sortArray.begin(), sortArray.end(), 0.0) / sortArray.size();

			if (((sortArray[3] - sortArray[0]) > tooSteep))
			{
				//int colourMult = 1;
				//float average = std::accumulate(sortArray.begin(), sortArray.end(), 0.0) / sortArray.size();
				//colourMult = average - rockHeight;


				colorArray[(pos * 4) + 0] = 25+average;
				colorArray[(pos * 4) + 1] = 25+average;
				colorArray[(pos * 4) + 2] = 25+average;
				colorArray[(pos * 4) + 3] = 255;

				//colorArray.push_back(25);
				//colorArray.push_back(25);
				//colorArray.push_back(25);
				//colorArray.push_back(255);
			}
			else if ((average >= rockHeight))
			{

				int colourMult = 1;
				//float average = std::accumulate(sortArray.begin(), sortArray.end(), 0.0) / sortArray.size();
				colourMult = average - rockHeight;


				colorArray[(pos * 4) + 0] = 50 + colourMult;
				colorArray[(pos * 4) + 1] = 50 + colourMult;
				colorArray[(pos * 4) + 2] = 50 + colourMult;
				colorArray[(pos * 4) + 3] = 255;

				//colorArray.push_back(25);
				//colorArray.push_back(25);
				//colorArray.push_back(25);
				//colorArray.push_back(255);


			}
			else
			{
				colorArray[(pos * 4) + 0] = 0;
				colorArray[(pos * 4) + 1] = 25+average;
				colorArray[(pos * 4) + 2] = 0;
				colorArray[(pos * 4) + 3] = 255;

				//colorArray.push_back(sf::Color::Green.r);
				//colorArray.push_back(sf::Color::Green.g);
				//colorArray.push_back(sf::Color::Green.b);
				//colorArray.push_back(255);
			}
			
			i++;
			sortArray.clear();
			pos++;
		}
		j++;
	} */
	mapSprite.create((rows-1),(cols-1),standardColourArray);
	delete standardColourArray;
	mapChanged = false;
	return mapSprite;
}

void Heightmap::setRoadInfo(std::vector<int>& tempPathInfo)
{
	sf::Clock clock; // starts the clock
	for (int i(0); i < tempPathInfo.size(); i++)
	{
		//std::div_t LocationData = std::div((tempPathInfo[i]) + ((tempPathInfo[i + 1])* (cols - 1)), rows);
		mapData[(tempPathInfo[i]) + ((tempPathInfo[i + 1])* (cols - 1))].hasRoad = true;
		//roadWalkability[tempPathInfo[i]][tempPathInfo[i + 1]] = 1;

		roadWalkability[indexTranslate(tempPathInfo[i],tempPathInfo[i + 1])] = 1;


		const int maxInterestCost = 255;
		const int radiusOfCost = 3;

		//std::vector<int> sortArray(4, 0);

		int roadX = tempPathInfo[i];
		int roadY = tempPathInfo[i + 1];

		int tempInterestInt(0);
		for (int x(-radiusOfCost); x <= radiusOfCost; x++)
		{
			for (int y(-radiusOfCost); y <= radiusOfCost; y++)
			{
				if ((((roadX + x) >= 0) && ((roadX + x) < cols-1))) //Check column out of bounds
					if ((((roadY + y) >= 0) && ((roadY + y) < rows-1))) //Check row out of bounds
					{
						float cost(abs(y));
						if (abs(x) > abs(y))
							cost = abs(x);

						cost = (radiusOfCost+1 - cost)*(maxInterestCost / radiusOfCost);

						if (roadInterestData[(roadX + x) + ((roadY + y)* (rows - 1))] < cost)
						{
							roadInterestData[(roadX + x) + ((roadY + y)* (rows - 1))] = cost;

							tempInterestInt = roadInterestData[(roadX + x) + ((roadY + y)* (rows - 1))];
							int pos = (roadX + x) + ((roadY + y)*(rows - 1));

							if (tempInterestInt > 0)
							{
								roadInterestColourArray[(pos * 4) + 0] = 0 + tempInterestInt;
								roadInterestColourArray[(pos * 4) + 1] = 0 + tempInterestInt;
								roadInterestColourArray[(pos * 4) + 2] = 0 + tempInterestInt;
								roadInterestColourArray[(pos * 4) + 3] = 127;
							}
							else //make invisible if not affecting any calculations
							{
								roadInterestColourArray[(pos * 4) + 0] = 0;
								roadInterestColourArray[(pos * 4) + 1] = 0;
								roadInterestColourArray[(pos * 4) + 2] = 0;
								roadInterestColourArray[(pos * 4) + 3] = 0;
							}
							//pos++;
	
						}
					}
			}

		}
		i++;
	}
	roadInterestChanged = true;
	roadInterestTime += clock.getElapsedTime().asSeconds();
	//std::cout << "Time Taken to generate Road Interest Map: " << clock.getElapsedTime().asSeconds() << std::endl;
}

void Heightmap::displayRoadInterestTime()
{
	std::cout << "Time Taken to generate Road Interest Map: " << roadInterestTime << std::endl;
}




void Heightmap::setBuildingInfo(std::vector<int>& tempPathInfo)
{

	for (int i(0); i < tempPathInfo.size(); i++)
	{
		//std::div_t LocationData = std::div((tempPathInfo[i]) + ((tempPathInfo[i + 1])* (cols - 1)), rows);
		//std::cout << "SundayToast: " << (tempPathInfo[i]) + ((tempPathInfo[i + 1])* (cols - 1)) << "\n";
		mapData[(tempPathInfo[i]) + ((tempPathInfo[i + 1])* (cols - 1))].hasBuilding = true;
		//roadWalkability[tempPathInfo[i]][tempPathInfo[i + 1]] = 1;
		roadWalkability[indexTranslate(tempPathInfo[i], tempPathInfo[i + 1])] = 1;

		i++;
	}
	//roadInterestChanged = true;
}

void Heightmap::startFillArea()
{
	std::vector<sf::Vector2i> edgeLocs;
	edgeLocs = getEdgeWalkability();
	std::vector<int> emptyVec;
	emptyVec.resize(((cols - 1) * (rows - 1)),1);
	std::vector<int> deleteVec;

	do
	{
		fillWalkability.push_back(emptyVec);
		fillArea(edgeLocs[0].x, edgeLocs[0].y, fillWalkability.size() - 1);

		for (int i(0); i < edgeVec.size() / 2; i++)
		{
			int tempX, tempY;
			tempX = edgeVec[(i * 2) + 0];
			tempY = edgeVec[(i * 2) + 1];
			for (int j(0); j < edgeLocs.size(); j++)
			{
				if (edgeLocs[j].x == tempX && edgeLocs[j].y == tempY)
					deleteVec.push_back(j);
			}
		}
		edgeLocs.erase(edgeLocs.begin());
		std::sort(deleteVec.begin(), deleteVec.end(), std::greater<int>()); //Sort into decending
		//std::sort(deleteVec)
		for (int j(0); j < deleteVec.size(); j++)
		{
			edgeLocs.erase(edgeLocs.begin() + deleteVec[j]);
		}


	} while (edgeLocs.size() > 0);


}


void Heightmap::fillArea(int startX, int startY, int fillIndex)
{




	/*
	if (startX < 0 || startX > cols - 1)
		return;
	if (startY < 0 || startY > rows - 1)
		return;

	if (walkability[startX][startY] == 1)
		return;
	if (fillWalkability[fillIndex][(startX)+((startY)* (cols - 1))] == 0)
		return;

	fillWalkability[fillIndex][(startX)+((startY)* (cols - 1))] = 0;

	//fillWalkability[fillIndex].push_back(startX);
	//fillWalkability[fillIndex].push_back(startY);

	if (startX == 0 || startX == cols - 1 ||
		startY == 0 || startY == rows - 1) //left-right columns
	{
		edgeVec.push_back(startX);
		edgeVec.push_back(startY);

	}
	fillArea(startX - 1, startY, fillIndex);//West
	fillArea(startX + 1, startY, fillIndex);//East
	fillArea(startX    , startY - 1, fillIndex);//North
	fillArea(startX    , startY + 1, fillIndex);//South

	return;

	//else
	//if () //top-bottom rows
	//{
	//	edgeVec.push_back(startX);
	//	edgeVec.push_back(startY);
	//}
	//int startX
	
	*/

}

void Heightmap::generateRoadConnections()
{
	int newConnectionIndex(0);
	std::vector<std::pair<int, int>> tempEdgeConnectionsVec;



	for (int i(0); i < edgeConnectionInfo.size(); i++)
	{
		//std::cout << "Connection 1 : " << edgeConnections[i].size() << " Connection 2 : " << edgeConnections[newConnectionIndex].size() << "\n";
		if (edgeConnectionInfo[i].size() > edgeConnectionInfo[newConnectionIndex].size()) //If more connections(Max would be 4 due to way edge/sides work // disregards same size connections for now
		{
			//std::cout << "Connection Has More Sides " << "\n";
			newConnectionIndex = i;
		}
	}

	int potentialConnections(1 + edgeConnectionInfo[newConnectionIndex].size()); // First index holds data for 2 connections, additonal indices hold one.

	//First two Connections
	tempEdgeConnectionsVec.push_back(edgeConnectionInfo[newConnectionIndex][0].firstEdge);
	tempEdgeConnectionsVec.push_back(edgeConnectionInfo[newConnectionIndex][0].secondEdge);

	//Additonal Connections
	for (int i(1); i < edgeConnectionInfo[newConnectionIndex].size(); i++) //Skip first index
	{
		tempEdgeConnectionsVec.push_back(edgeConnectionInfo[newConnectionIndex][i].secondEdge);
	}

	std::uniform_real_distribution<double> distConnections(2.0, potentialConnections); //Minimum of two edges, max of potentialConnections
	std::uniform_real_distribution<double> distConnections2(-0.5, potentialConnections+0.5); //Minimum of two edges, max of potentialConnections

	int numOfConnections = round(distConnections(mtRandomGenerator)); //Makes 4 connections rarer than 2-3 due to only 0.5 instead of usual 1.0 range of rounding.
	//std::cout << "Connections to be made : " << numOfConnections << "\n";
	numOfConnections = 3; //TEMP
	std::vector<int> orderOfConnections; //begin() - > end();


	//mtRandomGenerator.seed(23445324432);
	//mtRandomGenerator.seed(4345324432);
	//mtRandomGenerator.seed(434534534432);
	//mtRandomGenerator.seed(435335324434532);
	//mtRandomGenerator.seed(654324432);
	bool isFree(true);

	while (orderOfConnections.size() < numOfConnections)
	{
		
		int indexToChoose = round(distConnections2(mtRandomGenerator)); //Should be 0 - > numOfConnections-1

		if (indexToChoose < 0)
			indexToChoose = 0;
		else if (indexToChoose > numOfConnections-1)
			indexToChoose = numOfConnections-1;

		//Check if already chosen
		for (int i(0); i < orderOfConnections.size(); i++)
		{
			//int temp = orderOfConnections[i];
			if (orderOfConnections[i] == indexToChoose)
			{
				isFree = false;
			}
		}

		if (isFree) //Index not chosen
		{
			orderOfConnections.push_back(indexToChoose);
		}
		isFree = true;
	}

	//std::cout << "Index Chosen. \nOrder : " << orderOfConnections[0] << " " << orderOfConnections[1] << " "  << orderOfConnections[2] << "\n";

	roadAccessPoints.clear();
	for (int i(0); i < orderOfConnections.size(); i++)
	{
		sf::Vector2i tempVec;
		int index = orderOfConnections[i];
		int sideIndex = tempEdgeConnectionsVec[index].first;
		int edgeIndex = tempEdgeConnectionsVec[index].second;

		std::uniform_real_distribution<double> distTileData(0.0, edgeConnections[sideIndex][edgeIndex].size()); //TileData

		int tileIndex = distTileData(mtRandomGenerator);

		tempVec = edgeConnections[sideIndex][edgeIndex][tileIndex];


		roadAccessPoints.push_back(tempVec);
	}
	
	//std::cout << "First X: " << roadAccessPoints[0].x << " First Y : " << roadAccessPoints[0].y << "\n";
	//std::cout << "Second X: " << roadAccessPoints[1].x << " Second Y : " << roadAccessPoints[1].y << "\n";
	//std::cout << "Third X: " << roadAccessPoints[2].x << " Third Y : " << roadAccessPoints[2].y << "\n";


	/*
	sf::Vector2i start = edgeConnections[i][j][edgeConnections[i][j].size() / 2]; //use mid point of edge for path test
	sf::Vector2i end = edgeConnections[i][k][edgeConnections[i][k].size() / 2];
	std::vector<int> tempVec = findPath(start.x, start.y, end.x, end.y);
	//std::cout << "Edge Side : " << i <<" Edge : " << j << "Checked against : "<< k << "\n";

	if (tempVec.size() > 0) //if size is > 0, path is found.
	{
		std::cout << "Edge 1 Size : " << edgeConnections[i][j].size() << "\n";
		std::cout << "Edge 2 Size : " << edgeConnections[i][k].size() << "\n";
		edgeConnections[i][j].insert(edgeConnections[i][j].end(), edgeConnections[i][k].begin(), edgeConnections[i][k].end()); //add edge onto first edge
		std::cout << "Edge 1 Size After: " << edgeConnections[i][j].size() << "\n";
		edgeConnections[i][k].clear();
		std::cout << "Edge 2 Size After : " << edgeConnections[i][k].size() << "\n";

		//if (j + 1 != edgeConnections.size())//Not last Edge
		//edgeIndexChecked.push_back(i);
	}*/


}

std::vector<sf::Vector2i>* Heightmap::getRoadAccessPoints()
{

	return &roadAccessPoints;
}


void Heightmap::setSeed(double newSeed)
{
	heightmapSeed = newSeed;
	mtRandomGenerator.seed(heightmapSeed);

}
double Heightmap::getSeed()
{
	return heightmapSeed;

}




std::vector<sf::Vector2i> Heightmap::getEdgeWalkability()
{
	std::vector<sf::Vector2i> tempVecLocs; //holds walkability locations
	sf::Vector2i tempVec;
	//Easy hardcoded method
	//for (int i = 0; i < rows - 1; i++)
	//{
	//	if (walkability[i][0] == walkable)
	//	{
	//		tempVec.x = i;
	//		tempVec.y = 0;
	//		tempVecLocs.push_back(tempVec);
	//		//tempVecLocs.push_back(i);
	//		//tempVecLocs.push_back(0);
	//	}
	//	if (walkability[i][rows - 1] == walkable)
	//	{
	//		tempVec.x = i;
	//		tempVec.y = rows - 1;
	//		tempVecLocs.push_back(tempVec);
	//		//tempVecLocs.push_back(i);
	//		//tempVecLocs.push_back(cols-1);
	//	}
	//}
	//
	////Miss corners as they should have been handled with previous loop.
	//for (int i = 1; i < cols - 2; i++)
	//{
	//	if (walkability[0][i] == walkable)
	//	{
	//		tempVec.x = 0;
	//		tempVec.y = i;
	//		tempVecLocs.push_back(tempVec);
	//
	//		//tempVecLocs.push_back(0);
	//		//tempVecLocs.push_back(i);
	//	}
	//	if (walkability[i][cols-1] == walkable)
	//	{
	//		tempVec.x = cols - 1;
	//		tempVec.y = i;
	//		tempVecLocs.push_back(tempVec);
	//		//tempVecLocs.push_back(rows - 1);
	//		//tempVecLocs.push_back(i);
	//	}
	//}

	for (int i = 0; i < rows - 1; i++)
	{
		if (walkability[indexTranslate(i,0)] == walkable)
		{
			tempVec.x = i;
			tempVec.y = 0;
			tempVecLocs.push_back(tempVec);
			//tempVecLocs.push_back(i);
			//tempVecLocs.push_back(0);
		}
		if (walkability[indexTranslate(i, rows - 1)] == walkable)
		{
			tempVec.x = i;
			tempVec.y = rows - 1;
			tempVecLocs.push_back(tempVec);
			//tempVecLocs.push_back(i);
			//tempVecLocs.push_back(cols-1);
		}
	}

	//Miss corners as they should have been handled with previous loop.
	for (int i = 1; i < cols - 2; i++)
	{
		if (walkability[indexTranslate(0, i)])
		{
			tempVec.x = 0;
			tempVec.y = i;
			tempVecLocs.push_back(tempVec);

			//tempVecLocs.push_back(0);
			//tempVecLocs.push_back(i);
		}
		if (walkability[indexTranslate(cols-1, i)])
		{
			tempVec.x = cols - 1;
			tempVec.y = i;
			tempVecLocs.push_back(tempVec);
			//tempVecLocs.push_back(rows - 1);
			//tempVecLocs.push_back(i);
		}
	}



	return tempVecLocs;
}

std::vector<std::vector<std::vector<sf::Vector2i>>>* Heightmap::getEdgeVectors()
{
	return &edgeConnections;

}

void Heightmap::generateEdgeConnections()
{

	//Cleanup
	if (edgeConnections.size() > 0)
		edgeConnections.clear();
	edgeConnections.resize(4);


	//std::vector<std::vector<sf::Vector2i>> edgeVectors;
	std::vector<sf::Vector2i> tempVecLocs; //holds walkability locations
	std::vector<sf::Vector2i> tempVecLocs2; //holds walkability locations
	sf::Vector2i tempVec;
	bool currentlyWalkable[2] = { false, false };
	
	
	for (int i(0); i < cols - 1; i++)
	{
		/////// Top Line
		//if (walkability[i][0] == walkable) //[x][y]
		//{

		if (walkability[indexTranslate(i, 0)] == walkable) //[x][y]
		{

			currentlyWalkable[0] = true;
			tempVec.x = i;
			tempVec.y = 0;
			tempVecLocs.push_back(tempVec);
		}
		else
		{
			if (currentlyWalkable[0] == true)
			{
				//edgeConnections.push_back(std::make_pair(tempVecLocs, north));
				edgeConnections[north].push_back(tempVecLocs);
				//edgeVectors.push_back(tempVecLocs);
				tempVecLocs.clear();
			}
			currentlyWalkable[0] = false;
		}
		///////

		/////// Bottom Line
		//if (walkability[i][rows-2] == walkable)
		//{
		if (walkability[indexTranslate(i, rows-2)] == walkable)
		{
			currentlyWalkable[1] = true;
			tempVec.x = i;
			tempVec.y = rows - 2;
			tempVecLocs2.push_back(tempVec);
		}
		else
		{
			if (currentlyWalkable[1] == true)
			{
				edgeConnections[south].push_back(tempVecLocs2);
				//edgeVectors.push_back(tempVecLocs2);
				tempVecLocs2.clear();
			}
			currentlyWalkable[1] = false;
		}
		///////
	}
	//Edge case literally HA
	if (tempVecLocs.size() > 0)
	{ 
		edgeConnections[north].push_back(tempVecLocs);
		//edgeConnections.push_back(std::make_pair(tempVecLocs, north));
		//edgeVectors.push_back(tempVecLocs);
		tempVecLocs.clear();
	}

	if (tempVecLocs2.size() > 0)
	{
		edgeConnections[south].push_back(tempVecLocs2);
		//edgeConnections.push_back(std::make_pair(tempVecLocs2, south));
		//edgeVectors.push_back(tempVecLocs2);
		tempVecLocs2.clear();
	}

	currentlyWalkable[0] = false;
	currentlyWalkable[1] = false;
	
	for (int i(1); i < rows - 2; i++) //Column pass should have addressed edges //[x][y]
	{
		/////// Left Line
		//if (walkability[0][i] == walkable)
		//{
		/////// Left Line
		if (walkability[indexTranslate(0, i)] == walkable)
		{
			currentlyWalkable[0] = true;
			tempVec.x = 0;
			tempVec.y = i;
			tempVecLocs.push_back(tempVec);
		}
		else
		{
			if (currentlyWalkable[0] == true)
			{
				edgeConnections[west].push_back(tempVecLocs);
				//edgeConnections.push_back(std::make_pair(tempVecLocs, west));
				//edgeVectors.push_back(tempVecLocs);
				tempVecLocs.clear();
			}
			currentlyWalkable[0] = false;
		}
		///////

		/////// Right Line
		//if (walkability[cols - 2][i] == walkable)
		//{
		if (walkability[indexTranslate(cols-2, i)] == walkable)
		{
			currentlyWalkable[1] = true;
			tempVec.x = cols - 2;
			tempVec.y = i;
			tempVecLocs2.push_back(tempVec);
		}
		else
		{
			if (currentlyWalkable[1] == true)
			{
				edgeConnections[east].push_back(tempVecLocs);
				//edgeConnections.push_back(std::make_pair(tempVecLocs, east));
				//edgeVectors.push_back(tempVecLocs2);
				tempVecLocs2.clear();
			}
			currentlyWalkable[1] = false;
		}
		///////	
	}
	
	if (tempVecLocs.size() > 0)
	{
		edgeConnections[west].push_back(tempVecLocs);
		//edgeConnections.push_back(std::make_pair(tempVecLocs, west));
		//edgeVectors.push_back(tempVecLocs);
		tempVecLocs.clear();
	}

	if (tempVecLocs2.size() > 0)
	{
		edgeConnections[east].push_back(tempVecLocs2);
		//edgeConnections.push_back(std::make_pair(tempVecLocs2, east));
		//edgeVectors.push_back(tempVecLocs2);
		tempVecLocs2.clear();
	}

	//std::cout << "North Edges Found: " << edgeConnections[north].size() << "\n";
	//std::cout << "South Edges Found: " << edgeConnections[south].size() << "\n";
	//std::cout << "East Edges Found: " << edgeConnections[east].size() << "\n";
	//std::cout << "West Edges Found: " << edgeConnections[west].size() << "\n";
}

void Heightmap::testEdges()
{
	sf::Clock clock; // starts the clock
	//Test Edges against edges on same side
	for (int i(0); i < edgeConnections.size(); i++)
	{
		//int i = north;
		if (edgeConnections[i].size() > 0)//Check edges exist
		{
			for (int j(0); j < edgeConnections[i].size() - 1; j++) //Checks each side
			{
				int index = (j + 1);//No need ot test indexes before
				for (int k(index); k < edgeConnections[i].size(); k++) //Checks each side
				{
					//edgeConnections[i][j].size();
					//edgeConnections[i][k].size();
					if (edgeConnections[i][j].size() > 0 && edgeConnections[i][k].size() > 0)
					{

						sf::Vector2i start = edgeConnections[i][j][edgeConnections[i][j].size() / 2]; //use mid point of edge for path test
						sf::Vector2i end = edgeConnections[i][k][edgeConnections[i][k].size() / 2];
						std::vector<int> tempVec = findPath(start.x, start.y, end.x, end.y);
						//std::cout << "Edge Side : " << i <<" Edge : " << j << "Checked against : "<< k << "\n";

						if (tempVec.size() > 0) //if size is > 0, path is found.
						{
							//std::cout << "Edge 1 Size : " << edgeConnections[i][j].size() << "\n";
							//std::cout << "Edge 2 Size : " << edgeConnections[i][k].size() << "\n";
							edgeConnections[i][j].insert(edgeConnections[i][j].end(), edgeConnections[i][k].begin(), edgeConnections[i][k].end()); //add edge onto first edge
							//std::cout << "Edge 1 Size After: " << edgeConnections[i][j].size() << "\n";
							edgeConnections[i][k].clear();
							//std::cout << "Edge 2 Size After : " << edgeConnections[i][k].size() << "\n";

							//if (j + 1 != edgeConnections.size())//Not last Edge
							//edgeIndexChecked.push_back(i);
						}
					}
				} // k loop
			} // j loop
		} // size check

		//Remove empty vectors
		if (edgeConnections[i].size() > 0)//Check edges exist
		{
			for (int j(edgeConnections[i].size() - 1); j > 0; j--) //Checks each side
			{
				if (edgeConnections[i][j].size() == 0)
				{
					edgeConnections[i].erase(edgeConnections[i].begin() + j); //delete J
					//std::cout << "Side : " << i << " Edge : " << j << " Deleted " << "\n";
				}
			}

		}
	} // i loop

	//Checks for remaining Edges
	//std::cout << "North Edges : " << edgeConnections[north].size() << "\n";
	//std::cout << "South Edges : " << edgeConnections[south].size() << "\n";
	//std::cout << "East Edges : " << edgeConnections[east].size() << "\n";
	//std::cout << "West Edges : " << edgeConnections[west].size() << "\n";

	//Edge Check against Edges on different sides
	//if
	//Test Edges against edges on same side
	std::vector<std::vector<int>> checkVec;
	checkVec.resize(edgeConnections.size());
	//for (int i(0); i < tempVec.size(); i++)
	//{
	//	tempVec[i].resize(edgeConnections[i].size());
	//}

	//tempVec[north].size();
	bool breakCondition(false);

	for (int i(0); i < edgeConnections.size() - 1; i++) //First Side
	{

		if (edgeConnections[i].size() > 0)//Check edges exist
		{
			int index = i + 1;
			int startingSideCheck(0);
			int startingEdgeCheck(0);
			for (int k(0); k < edgeConnections[i].size(); k++) //First Sides edge
			{
				startingSideCheck = i;
				startingEdgeCheck = k;
				std::vector<edgeInfo> tempEdgeInfo;
				breakCondition = false;
				for (int l(0); l < checkVec[i].size(); l++)
				{
					if (k == checkVec[i][l]) //If Edge Checked and path found
					{
						//std::cout << "EDGE ALREADY CHECKED" << "\n";
						breakCondition = true;
						break;
					}
				}
				if (!breakCondition)
				{
					for (int j(index); j < edgeConnections.size(); j++) //check sides past first side e.g. North - > east,west,south: East - > west, south etc || Second side
					{

						for (int n(0); n < edgeConnections[j].size(); n++) //Second sides edge
						{
							//for (int l(0); l < checkVec[i].size(); l++)
							//{
							//	if (n == checkVec[i][l]) //If Edge Checked and path found
							//	{
							//		std::cout << "EDGE ALREADY CHECKED" << "\n";
							//		breakCondition = true;
							//		break;
							//	}
							//}
							if (breakCondition)
							{
								breakCondition = false;
								//break;
							}
							else
							{
								//sf::Vector2i start = edgeConnections[i][k][edgeConnections[i][k].size() / 2]; //use mid point of edge for path test
								sf::Vector2i start = edgeConnections[startingSideCheck][startingEdgeCheck][edgeConnections[startingSideCheck][startingEdgeCheck].size() / 2]; //use mid point of edge for path test
								sf::Vector2i end = edgeConnections[j][n][edgeConnections[j][n].size() / 2];
								//std::cout << "Finding Path Between Side : " << startingSideCheck << " Edge : " << startingEdgeCheck << " And Side : " << j << " Edge : " << n << "\n";
								std::vector<int> tempVec = findPath(start.x, start.y, end.x, end.y);
								//std::cout << "Edge Side : " << i <<" Edge : " << j << "Checked against : "<< k << "\n";

								if (tempVec.size() > 0) //if size is > 0, path is found.
								{
									//std::cout << "Path Found Between Side : " << startingSideCheck << " Edge : " << startingEdgeCheck << " And Side : " << j << " Edge : " << n << "\n";
									checkVec[j].push_back(n);//Edge Checked and succeed
									edgeInfo newEdgeConnection;
									newEdgeConnection.firstEdge = std::make_pair(startingSideCheck, startingEdgeCheck);//Side, Edge
									newEdgeConnection.secondEdge = std::make_pair(j, n);


									tempEdgeInfo.push_back(newEdgeConnection);//push connections into vector
									startingSideCheck = j; // Use found edge as start point to check new edges.
									startingEdgeCheck = n; // Use found edge as start point to check new edges.
									//breakCondition = true;
									break;
									//if (j + 1 != edgeConnections.size())//Not last Edge
									//edgeIndexChecked.push_back(i);
								}
							}
						}

					}
				}
				if (tempEdgeInfo.size() > 0 )
					edgeConnectionInfo.push_back(tempEdgeInfo);
			}
		}
	}


	if (edgeConnectionInfo.size() > 0)
	{
		sf::Clock clock; // starts the clock
		generateRoadConnections();
		std::cout << "Time Taken To Generate Road Connections : " << clock.getElapsedTime().asSeconds() << std::endl;
	}

}




void Heightmap::clearRoadInfo()
{
	for (int i(0); i < mapData.size(); i++)
	{
		mapData[i].hasRoad = false;
	}


}


std::vector<int> Heightmap::findPath(int startX, int startY, int targetX, int targetY)
{
	std::vector<int> pathInfo;

	sf::Clock clock; // starts the clock
	int ID = 1;
	//pathStatus[ID] = FindPath(ID, startX, startY,
	//	targetX, targetY, this);

	pathStatus[ID] = FindPath(ID, startX, startY,targetX, targetY);

	//std::cout << "Time Taken to find path: " << clock.getElapsedTime().asSeconds() << std::endl;

	////1. Find Path: If smiley and chaser are not at the same location on the 
	////	screen and no path is currently active, find a new path.
	//if (xLoc[ID] != xLoc[targetID] || yLoc[ID] != yLoc[targetID])
	//{
	//	//If no path has been generated, generate one. Update it when
	//	//the chaser reaches its fifth step on the current path.	
	//	if (pathStatus[ID] == notStarted || pathLocation[ID] == 5)
	//	{
	//		//Generate a new path. Enter coordinates of smiley sprite (xLoc(1)/
	//		//yLoc(1)) as the target.
	//		pathStatus[ID] = FindPath(ID, xLoc[ID], yLoc[ID],
	//			xLoc[targetID], yLoc[targetID]);
	//
	//	}
	//}
	
	int tempX, tempY;
	//if (pathStatus[ID] == found)
	//{

	if (pathStatus[ID] == found)
	{
		//mapData[(startX)+((startY)* (cols-1))].hasRoad = true;

		pathInfo.push_back(startX);
		pathInfo.push_back(startY);

		//std::cout << "Starting Point Avg Height: " << mapData[(startX)+((startY)* (cols - 1))].heightAverageVal << std::endl;
		//if (pathLocation[ID] < pathLength[ID])
		//{
		if (pathLocation[ID] < pathLength[ID])
		{
			//if just starting 
			//if (pathLocation[ID] == 0 )
			//	pathLocation[ID] = pathLocation[ID] + 1;

			if (pathLocation[ID] == 0)
				pathLocation[ID] = pathLocation[ID] + 1;
		}

		do
		{
			//tempX = ReadPathX(ID, pathLocation[ID]);
			//tempY = ReadPathY(ID, pathLocation[ID]);
			if (pathLocation[ID] <= pathLength[ID])
			{
				tempX = ReadPathX(ID, pathLocation[ID]);
				tempY = ReadPathY(ID, pathLocation[ID]);

				pathInfo.push_back(tempX);
				pathInfo.push_back(tempY);
			}
			//mapData[(tempX)+((tempY)* (cols-1))].hasRoad = true;

			

		//	if (pathLocation[ID] == pathLength[ID])
		//	{
		//		pathStatus[ID] = notStarted;
		//	}
		//	pathLocation[ID] = pathLocation[ID] + 1;
		//} while (pathLocation[ID] <= pathLength[ID]);
		//mapChanged = true;


			if (pathLocation[ID] == pathLength[ID])
			{
				pathStatus[ID] = notStarted;
			}
			pathLocation[ID] = pathLocation[ID] + 1;
		} while (pathLocation[ID] <= pathLength[ID]);
	mapChanged = true;

	}

	
	return pathInfo;
}

int Heightmap::getRows()
{
	return rows;

}
int Heightmap::getCols()
{
	return cols;

}


bool Heightmap::getMapChanged()
{
	return mapChanged;
}

bool Heightmap::getWaterInterestChanged()
{
	return waterInterestChanged;
}

bool Heightmap::getUnwalkableInterestChanged()
{
	return unwalkableInterestChanged;
}

bool Heightmap::getRoadInterestChanged()
{
	return roadInterestChanged;
}

bool Heightmap::getEdgeConnectChanged()
{
	return edgeConnectChanged;
}


std::vector<tileData>* Heightmap::getMapData()
{
	return &mapData;
}
std::vector<int>& Heightmap::getWaterInterestData()
{
	return waterInterestData;
}

std::vector<int>& Heightmap::getRoadInterestData()
{
	return roadInterestData;
}

std::vector<int>& Heightmap::getUnwalkableInterestData()
{
	return unwalkableInterestData;
}



vertexVals Heightmap::getVertexData(int location)
{
	std::div_t LocationData = std::div(location, rows);

	return vertexData[LocationData.quot*rows + LocationData.rem];

}

bool Heightmap::checkVillagePlacement(sf::Vector2i tempPos, int radius)
{
	int pos = 0;
	tileData tempData;
	tileData newTempData;
	bool isClear(true);
	int i = tempPos.x;
	int j = tempPos.y;
	tempData = mapData[(i)+((j)* (rows - 1))];

	int newRadius = radius;

	//if ((!tempData.isWater) || !(walkability[i][j + 1] == unwalkable) || !(roadWalkability[i][j + 1] == unwalkable))
	//{
	if ((!tempData.isWater) || !(walkability[indexTranslate(i, j + 1)] == unwalkable) || !(roadWalkability[indexTranslate(i, j + 1)] == unwalkable))
	{
		for (int x(-newRadius); x <= newRadius; x++)
		{
			for (int y(-newRadius); y <= newRadius; y++)
			{
				if ((((i + x) >= 0) && ((i + x) < cols))) //Check column out of bounds
					if ((((j + y) >= 0) && ((j + y) < rows))) //Check row out of bounds
					{
						newTempData = mapData[(i + x) + ((j + y)* (rows - 1))];
						//if ((newTempData.isWater) || (walkability[i + x][j + y] == unwalkable) || (roadWalkability[i + x][j + y] == unwalkable))
						//{
						if ((newTempData.isWater) || (walkability[indexTranslate(i + x, j + y)] == unwalkable) || (roadWalkability[indexTranslate(i+x,j+y)] == unwalkable))
						{
							isClear = false;
							return isClear;
						}
						}

			}

		}
	}
	return isClear;
}



sf::Image& Heightmap::generateWaterInterestMap()
{
	sf::Clock clock; // starts the clock
	//std::vector<sf::Uint8> colorArray;
	const int maxInterestCost = 255;
	const int radiusOfCost = 3;

	int arrayLength = ((cols - 1) * (rows - 1));
	waterInterestColourArray = new sf::Uint8[arrayLength * 4];
	//std::vector<int> sortArray(4, 0);

	//waterInterestData.resize(mapData.size());//Make same size as mapData



	int pos = 0;
	tileData tempData;
	tileData newTempData;
	for (int j = 0; j < rows - 1; j++)
	{
		for (int i = 0; i < cols - 1; i++)
		{
			tempData = mapData[(i)+((j)* (rows - 1))];

			if (tempData.isWater)
			{
				int blockedSides(0);
				const int maxSides(4);
				if (((i - 1) >= 0)) //Check column out of bounds)
					if (mapData[(i-1)+((j)* (rows - 1))].isWater)
						blockedSides++;
				if (((i + 1) < cols)) //Check column out of bounds)
					if (mapData[(i+1)+((j)* (rows - 1))].isWater)
						blockedSides++;

				if (((j - 1) >= 0)) //Check Rows out of bounds)
					if (mapData[(i)+((j-1)* (rows - 1))].isWater)
						blockedSides++;

				if (((j + 1) < rows)) //Check rowsS out of bounds)
					if (mapData[(i)+((j+1)* (rows - 1))].isWater)
						blockedSides++;

				if (!(blockedSides == maxSides))
				{

					for (int x(-radiusOfCost); x <= radiusOfCost; x++)
					{
						for (int y(-radiusOfCost); y <= radiusOfCost; y++)
						{
							if ((((i + x) >= 0) && ((i + x) < cols-1))) //Check column out of bounds
								if ((((j + y) >= 0) && ((j + y) < rows-1))) //Check row out of bounds
								{
								newTempData = mapData[(i + x) + ((j + y)* (rows - 1))];
								if (!newTempData.isWater)
									{
										int cost(abs(y));
										if (abs(x) > abs(y))
											cost = abs(x);

										cost = (radiusOfCost+1 - cost)*(maxInterestCost / radiusOfCost);

										if (waterInterestData[(i + x) + ((j + y)* (rows - 1))] < cost)
										{
											waterInterestData[(i + x) + ((j + y)* (rows - 1))] = cost;
										}
									}
								}
						}
					}
				}
			}
		}
	}

	int tempInterestInt(0);

	for (int j = 0; j < rows - 1; j++)
	{
		for (int i = 0; i < cols - 1; i++)
		{
			tempInterestInt = waterInterestData[(i)+((j)* (rows - 1))];

			if (tempInterestInt > 0)
			{
				waterInterestColourArray[(pos * 4) + 0] = 0 + tempInterestInt;
				waterInterestColourArray[(pos * 4) + 1] = 0 + tempInterestInt;
				waterInterestColourArray[(pos * 4) + 2] = 0 + tempInterestInt;
				waterInterestColourArray[(pos * 4) + 3] = 127;
			}
			else //make invisible if not affecting any calculations
			{
				waterInterestColourArray[(pos * 4) + 0] = 0;
				waterInterestColourArray[(pos * 4) + 1] = 0;
				waterInterestColourArray[(pos * 4) + 2] = 0;
				waterInterestColourArray[(pos * 4) + 3] = 0;
			}
			pos++;
		}
	}


		waterInterestSprite.create((rows - 1), (cols - 1), waterInterestColourArray);
		delete waterInterestColourArray;
		waterInterestChanged = false;


		std::cout << "Time Taken to generate Water Interest Map: " << clock.getElapsedTime().asSeconds() << std::endl;

		return waterInterestSprite;
		//return mapSprite;
}

sf::Image& Heightmap::generateUnwalkableInterestMap()
{
	sf::Clock clock; // starts the clock
	//std::vector<sf::Uint8> colorArray;
	const int maxInterestCost = 255;
	const int radiusOfCost = 10;

	int arrayLength = ((cols - 1) * (rows - 1));
	unwalkableInterestColourArray = new sf::Uint8[arrayLength * 4];
	//std::vector<int> sortArray(4, 0);

	//waterInterestData.resize(mapData.size());//Make same size as mapData



	int pos = 0;
	tileData tempData;
	tileData newTempData;
	for (int j = 0; j < rows - 1; j++)
	{
		for (int i = 0; i < cols - 1; i++)
		{
			tempData = mapData[(i)+((j)* (rows - 1))];
			
			//if (walkability[i][j] == unwalkable)
			//{
			if (walkability[indexTranslate(i,j)] == unwalkable)
			{
				//int blockedSides(0);
				//const int maxSides(4);
				//if (((i - 1) >= 0)) //Check column out of bounds)
				//	if (walkability[i-1][j] == unwalkable)
				//		blockedSides++;
				//if (((i + 1) < cols)) //Check column out of bounds)
				//	if (walkability[i + 1][j] == unwalkable)
				//		blockedSides++;
				//
				//if (((j - 1) >= 0)) //Check Rows out of bounds)
				//	if (walkability[i][j - 1] == unwalkable)
				//		blockedSides++;
				//
				//if (((j + 1) < rows)) //Check rowsS out of bounds)
				//	if (walkability[i][j + 1] == unwalkable)
				//		blockedSides++;

				int blockedSides(0);
				const int maxSides(4);
				if (((i - 1) >= 0)) //Check column out of bounds)
					if (walkability[indexTranslate(i-1, j)] == unwalkable)
						blockedSides++;
				if (((i + 1) < cols)) //Check column out of bounds)
					if (walkability[indexTranslate(i+1, j)] == unwalkable)
						blockedSides++;

				if (((j - 1) >= 0)) //Check Rows out of bounds)
					if (walkability[indexTranslate(i, j-1)] == unwalkable)
						blockedSides++;

				if (((j + 1) < rows)) //Check rowsS out of bounds)
					if (walkability[indexTranslate(i, j+1)] == unwalkable)
						blockedSides++;



				if (!(blockedSides == maxSides))
				{

					for (int x(-radiusOfCost); x <= radiusOfCost; x++)
					{
						for (int y(-radiusOfCost); y <= radiusOfCost; y++)
						{
							if ((((i + x) >= 0) && ((i + x) < cols-1))) //Check column out of bounds
								if ((((j + y) >= 0) && ((j + y) < rows-1))) //Check row out of bounds
								{
								//newTempData = mapData[(i + x) + ((j + y)* (rows - 1))];
								//if (walkability[i+x][j+y] == walkable)
								//{
								if (walkability[indexTranslate(i+x, j+y)] == walkable)
								{
									int cost(abs(y));
									if (abs(x) > abs(y))
										cost = abs(x);

									cost = (radiusOfCost + 1 - cost)*(maxInterestCost / radiusOfCost);

									if (unwalkableInterestData[(i + x) + ((j + y)* (rows - 1))] < cost)
									{
										unwalkableInterestData[(i + x) + ((j + y)* (rows - 1))] = cost;
									}
								}
								}
						}
					}
				}
			}
		}
	}

	int tempInterestInt(0);

	for (int j = 0; j < rows - 1; j++)
	{
		for (int i = 0; i < cols - 1; i++)
		{
			tempInterestInt = unwalkableInterestData[(i)+((j)* (rows - 1))];

			if (tempInterestInt > 0)
			{
				unwalkableInterestColourArray[(pos * 4) + 0] = 0 + tempInterestInt;
				unwalkableInterestColourArray[(pos * 4) + 1] = 0 + tempInterestInt;
				unwalkableInterestColourArray[(pos * 4) + 2] = 0 + tempInterestInt;
				unwalkableInterestColourArray[(pos * 4) + 3] = 127;
			}
			else //make invisible if not affecting any calculations
			{
				unwalkableInterestColourArray[(pos * 4) + 0] = 0;
				unwalkableInterestColourArray[(pos * 4) + 1] = 0;
				unwalkableInterestColourArray[(pos * 4) + 2] = 0;
				unwalkableInterestColourArray[(pos * 4) + 3] = 0;
			}
			pos++;
		}
	}


	unwalkableInterestSprite.create((rows - 1), (cols - 1), unwalkableInterestColourArray);
	delete unwalkableInterestColourArray;
	unwalkableInterestChanged = false;


	std::cout << "Time Taken to generate Unwalkable Interest Map: " << clock.getElapsedTime().asSeconds() << std::endl;

	return unwalkableInterestSprite;
	//return mapSprite;
}


sf::Image& Heightmap::generateRoadInterestMap()
{
	

	int arrayLength = ((cols - 1) * (rows - 1));
	//roadInterestColourArray = new sf::Uint8[arrayLength * 4];


	int tempInterestInt(0);
	int pos = 0;

	/*for (int j = 0; j < rows - 1; j++)
	{
		for (int i = 0; i < cols - 1; i++)
		{
			tempInterestInt = roadInterestData[(i)+((j)* (rows - 1))];

			if (tempInterestInt > 0)
			{
				roadInterestColourArray[(pos * 4) + 0] = 0 + tempInterestInt;
				roadInterestColourArray[(pos * 4) + 1] = 0 + tempInterestInt;
				roadInterestColourArray[(pos * 4) + 2] = 0 + tempInterestInt;
				roadInterestColourArray[(pos * 4) + 3] = 127;
			}
			else //make invisible if not affecting any calculations
			{
				roadInterestColourArray[(pos * 4) + 0] = 0;
				roadInterestColourArray[(pos * 4) + 1] = 0;
				roadInterestColourArray[(pos * 4) + 2] = 0;
				roadInterestColourArray[(pos * 4) + 3] = 0;
			}
			pos++;
		}
	}*/

	roadInterestSprite.create((rows - 1), (cols - 1), roadInterestColourArray);
	//delete roadInterestColourArray;
	roadInterestChanged = false;


	

	return roadInterestSprite;

}

sf::Image& Heightmap::generateEdgeConnectMap()
{
	//std::vector<std::vector<sf::Vector2i>> edgeVec =  getEdgeVectors();
	generateEdgeConnections();
	sf::Clock clock; // starts the clock

	int arrayLength = ((cols - 1) * (rows - 1));
	edgeConnectColourArray = new sf::Uint8[arrayLength * 4];
	int tempX, tempY;

	int tempInterestInt(0);
	int pos = 0;
	if (edgeConnections.size() != 0)
		for (int k(0); k < edgeConnections.size(); k++)
		for (int i(0); i < edgeConnections[k].size(); i++)
		{
			if (edgeConnections[k][i].size() != 0)
				for (int j(0); j < edgeConnections[k][i].size(); j++)
				{
					tempX = edgeConnections[k][i][j].x;
					tempY = edgeConnections[k][i][j].y;

					int pos = (tempX) + ((tempY)*(rows - 1));

					edgeConnectColourArray[(pos * 4) + 0] = 255;
					edgeConnectColourArray[(pos * 4) + 1] = 90;
					edgeConnectColourArray[(pos * 4) + 2] = 90;
					edgeConnectColourArray[(pos * 4) + 3] = 190;


				}

		}

	//int pos = (roadX + x) + ((roadY + y)*(rows - 1));


	edgeConnectSprite.create((rows - 1), (cols - 1), edgeConnectColourArray);
	//delete roadInterestColourArray;
	edgeConnectChanged = false;


	std::cout << "Time Taken to generate Edge Connect Map: " << clock.getElapsedTime().asSeconds() << std::endl;

	return edgeConnectSprite;

}

inline int Heightmap::indexTranslate(int xLoc, int yLoc)
{
	return (xLoc + (yLoc * (mapHeight-1)));
}



//-----------------------------------------------------------------------------
// Name: InitializePathfinder
// Desc: Allocates memory for the pathfinder.
//-----------------------------------------------------------------------------
void Heightmap::InitializePathfinder(const int width, const int height)
{
	//Create needed arrays
	//char walkability [mapWidth][mapHeight];
	//char roadWalkability[mapWidth][mapHeight];
	//int	 openList[mapWidth*mapHeight+2]; //1 dimensional array holding ID# of open list items
	//int	 whichList[mapWidth+1][mapHeight+1];  //2 dimensional array used to record 
	// 	//	 whether a cell is on the open list or on the closed list.
	//int	 openX[mapWidth*mapHeight+2]; //1d array stores the x location of an item on the open list
	//int	 openY[mapWidth*mapHeight+2]; //1d array stores the y location of an item on the open list
	//int	 parentX[mapWidth+1][mapHeight+1]; //2d array to store parent of each cell (x)
	//int	 parentY[mapWidth+1][mapHeight+1]; //2d array to store parent of each cell (y)
	//int	 Fcost[mapWidth*mapHeight+2];	//1d array to store F cost of a cell on the open list
	//int	 Gcost[mapWidth+1][mapHeight+1]; 	//2d array to store G cost for each cell.
	//int	 Hcost[mapWidth*mapHeight+2];	//1d array to store H cost of a cell on the open list
	//int	 pathLength[numberPeople+1];     //stores length of the found path for critter
	//int	 pathLocation[numberPeople+1];   //stores current position along the chosen path for critter		
	//int* pathBank [numberPeople+1];

	mapWidth = width-1;
	mapHeight = height-1;



	walkability.resize(mapWidth*mapHeight);
	roadWalkability.resize(mapWidth*mapHeight);


	openList.resize(mapWidth * mapHeight + 2);
	openX.resize(mapWidth * mapHeight + 2);
	openY.resize(mapWidth * mapHeight + 2);
	Fcost.resize(mapWidth * mapHeight + 2);
	Hcost.resize(mapWidth * mapHeight + 2);


	whichList.resize((mapWidth + 1)*(mapHeight + 1));
	parentX.resize((mapWidth + 1)*(mapHeight + 1));
	parentY.resize((mapWidth + 1)*(mapHeight + 1));
	Gcost.resize((mapWidth + 1)*(mapHeight + 1));

	pathLength.resize(numberPeople + 1);
	pathLocation.resize(numberPeople + 1);
	pathBank.resize(numberPeople + 1);
	pathStatus.resize(numberPeople + 1);
	xPath.resize(numberPeople + 1);
	yPath.resize(numberPeople + 1);





}


//-----------------------------------------------------------------------------
// Name: EndPathfinder
// Desc: Frees memory used by the pathfinder.
//-----------------------------------------------------------------------------
void Heightmap::EndPathfinder(void)
{
}


//-----------------------------------------------------------------------------
// Name: FindPath
// Desc: Finds a path using A*
//-----------------------------------------------------------------------------
int Heightmap::FindPath(int pathfinderID, int startingX, int startingY,
	int targetX, int targetY)
{
	//std::vector<tileData>* mapDataRef = (heightmapPTR->getMapData());
	//std::vector<int>& waterDataRef = (heightmapPTR->getWaterInterestData());
	//std::vector<int>& roadDataRef = (heightmapPTR->getRoadInterestData());
	//std::vector<int>& unwalkableDataRef = (heightmapPTR->getUnwalkableInterestData());

	int onOpenList = 0, parentXval = 0, parentYval = 0,
		a = 0, b = 0, m = 0, u = 0, v = 0, temp = 0, corner = 0, numberOfOpenListItems = 0,
		addedGCost = 0, tempGcost = 0, path = 0,
		tempx, pathX, pathY, cellPosition,
		newOpenListItemID = 0;

	//1. Convert location data (in pixels) to coordinates in the walkability array.
	//int startX = startingX/tileSize;
	//int startY = startingY/tileSize;	
	//targetX = targetX/tileSize;
	//targetY = targetY/tileSize;

	int startX = startingX;
	int startY = startingY;
	targetX = targetX;
	targetY = targetY;

	//2.Quick Path Checks: Under the some circumstances no path needs to
	//	be generated ...

	//	If starting location and target are in the same location...
	if (startX == targetX && startY == targetY && pathLocation[pathfinderID] > 0)
		return found;
	if (startX == targetX && startY == targetY && pathLocation[pathfinderID] == 0)
		return nonexistent;

	//	If target square is unwalkable, return that it's a nonexistent path.
	//if (walkability[targetX][targetY] == unwalkable)
	//	goto noPath;

	if (walkability[indexTranslate(targetX, targetY)] == unwalkable)
		goto noPath;

	//3.Reset some variables that need to be cleared
	if (onClosedList > 1000000) //reset whichList occasionally
	{
		for (int x = 0; x < mapWidth; x++) {
			for (int y = 0; y < mapHeight; y++)
				whichList[indexTranslate(x, y)] = 0;
		}
		onClosedList = 10;
	}
	onClosedList = onClosedList + 2; //changing the values of onOpenList and onClosed list is faster than redimming whichList() array
	onOpenList = onClosedList - 1;
	pathLength[pathfinderID] = notStarted;//i.e, = 0
	pathLocation[pathfinderID] = notStarted;//i.e, = 0

	//Gcost[startX][startY] = 0; //reset starting square's G value to 0

	Gcost[indexTranslate(startX, startY)] = 0; //reset starting square's G value to 0


	//4.Add the starting location to the open list of squares to be checked.
	numberOfOpenListItems = 1;
	openList[1] = 1;//assign it as the top (and currently only) item in the open list, which is maintained as a binary heap (explained below)
	openX[1] = startX; openY[1] = startY;

	//5.Do the following until a path is found or deemed nonexistent.
	do
	{

		//6.If the open list is not empty, take the first cell off of the list.
		//	This is the lowest F cost cell on the open list.
		if (numberOfOpenListItems != 0)
		{

			//7. Pop the first item off the open list.
			parentXval = openX[openList[1]];
			parentYval = openY[openList[1]]; //record cell coordinates of the item
			//whichList[parentXval][parentYval] = onClosedList;//add the item to the closed list
			whichList[indexTranslate(parentXval, parentYval)] = onClosedList;//add the item to the closed list


			//	Open List = Binary Heap: Delete this item from the open list, which
			//  is maintained as a binary heap. For more information on binary heaps, see:
			//	http://www.policyalmanac.org/games/binaryHeaps.htm
			numberOfOpenListItems = numberOfOpenListItems - 1;//reduce number of open list items by 1	

			//	Delete the top item in binary heap and reorder the heap, with the lowest F cost item rising to the top.
			openList[1] = openList[numberOfOpenListItems + 1];//move the last item in the heap up to slot #1
			v = 1;

			//	Repeat the following until the new item in slot #1 sinks to its proper spot in the heap.
			do
			{
				u = v;
				if (2 * u + 1 <= numberOfOpenListItems) //if both children exist
				{
					//Check if the F cost of the parent is greater than each child.
					//Select the lowest of the two children.
					if (Fcost[openList[u]] >= Fcost[openList[2 * u]])
						v = 2 * u;
					if (Fcost[openList[v]] >= Fcost[openList[2 * u + 1]])
						v = 2 * u + 1;
				}
				else
				{
					if (2 * u <= numberOfOpenListItems) //if only child #1 exists
					{
						//Check if the F cost of the parent is greater than child #1	
						if (Fcost[openList[u]] >= Fcost[openList[2 * u]])
							v = 2 * u;
					}
				}

				if (u != v) //if parent's F is > one of its children, swap them
				{
					temp = openList[u];
					openList[u] = openList[v];
					openList[v] = temp;
				}
				else
					break; //otherwise, exit loop

			} while (true);//reorder the binary heap
			//while (!KeyDown(27));//reorder the binary heap


			//7.Check the adjacent squares. (Its "children" -- these path children
			//	are similar, conceptually, to the binary heap children mentioned
			//	above, but don't confuse them. They are different. Path children
			//	are portrayed in Demo 1 with grey pointers pointing toward
			//	their parents.) Add these adjacent child squares to the open list
			//	for later consideration if appropriate (see various if statements
			//	below).
			for (b = parentYval - 1; b <= parentYval + 1; b++){
				for (a = parentXval - 1; a <= parentXval + 1; a++){

					//	If not off the map (do this first to avoid array out-of-bounds errors)
					if (a != -1 && b != -1 && a != mapWidth && b != mapHeight){

						//	If not already on the closed list (items on the closed list have
						//	already been considered and can now be ignored).			
						//if (whichList[a][b] != onClosedList) {
						if (whichList[indexTranslate(a, b)] != onClosedList) {

							//	If not a wall/obstacle square.
							//if (walkability[a][b] != unwalkable) {
							if (walkability[indexTranslate(a, b)] != unwalkable) {

								//if (roadWalkability[a][b] != unwalkable || (a == targetX && b == targetY)) {
								if (roadWalkability[indexTranslate(a, b)] != unwalkable || (a == targetX && b == targetY)) {
									//	Don't cut across corners
									corner = walkable;

									if (a == parentXval - 1)
									{
										if (b == parentYval - 1)
										{
											//if (walkability[parentXval - 1][parentYval] == unwalkable
											//	|| walkability[parentXval][parentYval - 1] == unwalkable
											//	|| roadWalkability[parentXval - 1][parentYval] == unwalkable
											//	|| roadWalkability[parentXval][parentYval - 1] == unwalkable)

											if (walkability[indexTranslate(parentXval - 1, parentYval)] == unwalkable
												|| walkability[indexTranslate(parentXval, parentYval - 1)] == unwalkable
												|| roadWalkability[indexTranslate(parentXval - 1, parentYval)] == unwalkable
												|| roadWalkability[indexTranslate(parentXval, parentYval - 1)] == unwalkable)
												corner = unwalkable;
										}
										else if (b == parentYval + 1)
										{
											//if (walkability[parentXval][parentYval + 1] == unwalkable
											//	|| walkability[parentXval - 1][parentYval] == unwalkable
											//	|| roadWalkability[parentXval][parentYval + 1] == unwalkable
											//	|| roadWalkability[parentXval - 1][parentYval] == unwalkable)
											//	corner = unwalkable;

											if (walkability[indexTranslate(parentXval, parentYval + 1)] == unwalkable
												|| walkability[indexTranslate(parentXval - 1, parentYval)] == unwalkable
												|| roadWalkability[indexTranslate(parentXval, parentYval + 1)] == unwalkable
												|| roadWalkability[indexTranslate(parentXval - 1, parentYval)] == unwalkable)
												corner = unwalkable;
										}
									}
									else if (a == parentXval + 1)
									{
										if (b == parentYval - 1)
										{
											//if (walkability[parentXval][parentYval - 1] == unwalkable
											//	|| walkability[parentXval + 1][parentYval] == unwalkable
											//	|| roadWalkability[parentXval][parentYval - 1] == unwalkable
											//	|| roadWalkability[parentXval + 1][parentYval] == unwalkable)
											//	corner = unwalkable;

											if (walkability[indexTranslate(parentXval, parentYval - 1)] == unwalkable
												|| walkability[indexTranslate(parentXval + 1, parentYval)] == unwalkable
												|| roadWalkability[indexTranslate(parentXval, parentYval - 1)] == unwalkable
												|| roadWalkability[indexTranslate(parentXval + 1, parentYval)] == unwalkable)
												corner = unwalkable;


										}
										else if (b == parentYval + 1)
										{
											//if (walkability[parentXval + 1][parentYval] == unwalkable
											//	|| walkability[parentXval][parentYval + 1] == unwalkable
											//	|| roadWalkability[parentXval + 1][parentYval] == unwalkable
											//	|| roadWalkability[parentXval][parentYval + 1] == unwalkable)
											//	corner = unwalkable;

											if (walkability[indexTranslate(parentXval + 1, parentYval)] == unwalkable
												|| walkability[indexTranslate(parentXval, parentYval + 1)] == unwalkable
												|| roadWalkability[indexTranslate(parentXval + 1, parentYval)] == unwalkable
												|| roadWalkability[indexTranslate(parentXval, parentYval + 1)] == unwalkable)
												corner = unwalkable;
										}
									}
									if (corner == walkable) {

										//	If not already on the open list, add it to the open list.			
										//if (whichList[a][b] != onOpenList)
										//{

										if (whichList[indexTranslate(a, b)] != onOpenList)
										{

											//Create a new open list item in the binary heap.
											newOpenListItemID = newOpenListItemID + 1; //each new item has a unique ID #
											m = numberOfOpenListItems + 1;
											openList[m] = newOpenListItemID;//place the new open list item (actually, its ID#) at the bottom of the heap
											openX[newOpenListItemID] = a;
											openY[newOpenListItemID] = b;//record the x and y coordinates of the new item

											//Figure out its G cost
											if (abs(a - parentXval) == 1 && abs(b - parentYval) == 1)
												addedGCost = 14;//cost of going to diagonal squares	
											else
												addedGCost = 10;//cost of going to non-diagonal squares	

											//int steepnessCost = calcSteepness(parentXval, parentYval, a, b, *mapDataRef);
											//int waterCost = calcWaterInterestCost(a, b, waterDataRef);
											//int roadCost = calcRoadInterestCost(a, b, roadDataRef);
											//int unwalkableCost = calcUnwalkableInterestCost(a, b, unwalkableDataRef);

											int steepnessCost = calcSteepness(parentXval, parentYval, a, b);
											int waterCost = calcWaterInterestCost(a, b);
											int roadCost = calcRoadInterestCost(a, b);
											int unwalkableCost = calcUnwalkableInterestCost(a, b);

											//Gcost[a][b] = Gcost[parentXval][parentYval] + addedGCost + steepnessCost + waterCost + roadCost + unwalkableCost;
											Gcost[indexTranslate(a, b)] = Gcost[indexTranslate(parentXval, parentYval)] + addedGCost + steepnessCost + waterCost + roadCost + unwalkableCost;

											//Figure out its H and F costs and parent
											//Hcost[openList[m]] = 10*(abs(a - targetX) + abs(b - targetY));

											float xDistance = abs(a - targetX);
											float yDistance = abs(b - targetY);
											if (xDistance > yDistance)
											{
												Hcost[openList[m]] = 14 * yDistance + 10 * (xDistance - yDistance);
											}
											else
											{
												Hcost[openList[m]] = 14 * xDistance + 10 * (yDistance - xDistance);
											}


											//Fcost[openList[m]] = Gcost[a][b] + Hcost[openList[m]];
											//parentX[a][b] = parentXval; parentY[a][b] = parentYval;

											Fcost[openList[m]] = Gcost[indexTranslate(a, b)] + Hcost[openList[m]];
											parentX[indexTranslate(a, b)] = parentXval; parentY[indexTranslate(a, b)] = parentYval;

											//Move the new open list item to the proper place in the binary heap.
											//Starting at the bottom, successively compare to parent items,
											//swapping as needed until the item finds its place in the heap
											//or bubbles all the way to the top (if it has the lowest F cost).
											while (m != 1) //While item hasn't bubbled to the top (m=1)	
											{
												//Check if child's F cost is < parent's F cost. If so, swap them.	
												if (Fcost[openList[m]] <= Fcost[openList[m / 2]])
												{
													temp = openList[m / 2];
													openList[m / 2] = openList[m];
													openList[m] = temp;
													m = m / 2;
												}
												else
													break;
											}
											numberOfOpenListItems = numberOfOpenListItems + 1;//add one to the number of items in the heap

											//Change whichList to show that the new item is on the open list.
											//whichList[a][b] = onOpenList;
											whichList[indexTranslate(a, b)] = onOpenList;
										}

										//8.If adjacent cell is already on the open list, check to see if this 
										//	path to that cell from the starting location is a better one. 
										//	If so, change the parent of the cell and its G and F costs.	
										else //If whichList(a,b) = onOpenList
										{

											//Figure out the G cost of this possible new path
											if (abs(a - parentXval) == 1 && abs(b - parentYval) == 1)
												addedGCost = 14;//cost of going to diagonal tiles	
											else
												addedGCost = 10;//cost of going to non-diagonal tiles	

											//int steepnessCost = calcSteepness(parentXval, parentYval, a, b, *mapDataRef);
											//int waterCost = calcWaterInterestCost(a, b, waterDataRef);
											//int roadCost = calcRoadInterestCost(a, b, roadDataRef);
											//int unwalkableCost = calcUnwalkableInterestCost(a, b, unwalkableDataRef);

											int steepnessCost = calcSteepness(parentXval, parentYval, a, b);
											int waterCost = calcWaterInterestCost(a, b);
											int roadCost = calcRoadInterestCost(a, b);
											int unwalkableCost = calcUnwalkableInterestCost(a, b);

											//tempGcost = Gcost[parentXval][parentYval] + addedGCost + steepnessCost + waterCost + roadCost + unwalkableCost;
											tempGcost = Gcost[indexTranslate(parentXval, parentYval)] + addedGCost + steepnessCost + waterCost + roadCost + unwalkableCost;

											//If this path is shorter (G cost is lower) then change
											//the parent cell, G cost and F cost. 		
											//if (tempGcost < Gcost[a][b]) //if G cost is less,
											//{
											//	parentX[a][b] = parentXval; //change the square's parent
											//	parentY[a][b] = parentYval;
											//	Gcost[a][b] = tempGcost;//change the G cost			

											if (tempGcost < Gcost[indexTranslate(a, b)]) //if G cost is less,
											{
												parentX[indexTranslate(a, b)] = parentXval; //change the square's parent
												parentY[indexTranslate(a, b)] = parentYval;
												Gcost[indexTranslate(a, b)] = tempGcost;//change the G cost			

												//Because changing the G cost also changes the F cost, if
												//the item is on the open list we need to change the item's
												//recorded F cost and its position on the open list to make
												//sure that we maintain a properly ordered open list.
												for (int x = 1; x <= numberOfOpenListItems; x++) //look for the item in the heap
												{
													if (openX[openList[x]] == a && openY[openList[x]] == b) //item found
													{
														//Fcost[openList[x]] = Gcost[a][b] + Hcost[openList[x]];//change the F cost
														Fcost[openList[x]] = Gcost[indexTranslate(a, b)] + Hcost[openList[x]];//change the F cost

														//See if changing the F score bubbles the item up from it's current location in the heap
														m = x;
														while (m != 1) //While item hasn't bubbled to the top (m=1)	
														{
															//Check if child is < parent. If so, swap them.	
															if (Fcost[openList[m]] < Fcost[openList[m / 2]])
															{
																temp = openList[m / 2];
																openList[m / 2] = openList[m];
																openList[m] = temp;
																m = m / 2;
															}
															else
																break;
														}
														break; //exit for x = loop
													} //If openX(openList(x)) = a
												} //For x = 1 To numberOfOpenListItems
											}//If tempGcost < Gcost(a,b)

										}//else If whichList(a,b) = onOpenList	
									}//If not cutting a corner
								}//if not a road
							}//If not a wall/obstacle square.
						}//If not already on the closed list 
					}//If not off the map
				}//for (a = parentXval-1; a <= parentXval+1; a++){
			}//for (b = parentYval-1; b <= parentYval+1; b++){

		}//if (numberOfOpenListItems != 0)

		//9.If open list is empty then there is no path.	
		else
		{
			path = nonexistent; break;
		}

		//If target is added to open list then path has been found.
		//if (whichList[targetX][targetY] == onOpenList)
		//{
		if (whichList[indexTranslate(targetX, targetY)] == onOpenList)
		{
			path = found; break;
		}

	} while (1);//Do until path is found or deemed nonexistent

	//10.Save the path if it exists.
	if (path == found)
	{

		//a.Working backwards from the target to the starting location by checking
		//	each cell's parent, figure out the length of the path.
		pathX = targetX; pathY = targetY;
		do
		{
			//Look up the parent of the current cell.	
			//tempx = parentX[pathX][pathY];
			//pathY = parentY[pathX][pathY];
			tempx = parentX[indexTranslate(pathX, pathY)];
			pathY = parentY[indexTranslate(pathX, pathY)];
			pathX = tempx;

			//Figure out the path length
			pathLength[pathfinderID] = pathLength[pathfinderID] + 1;
		} while (pathX != startX || pathY != startY);

		////b.Resize the data bank to the right size in bytes
		//pathBank[pathfinderID] = (int*)realloc(pathBank[pathfinderID],
		//	pathLength[pathfinderID] * 8);

		//b.Resize the data bank to the right size in bytes
		//pathBank[pathfinderID] = (int*)realloc(pathBank[pathfinderID],
		//	pathLength[pathfinderID] * 8);
		//b.Resize the data bank
		pathBank[pathfinderID].clear();
		pathBank[pathfinderID].resize(pathLength[pathfinderID] * 2); //*2 because algor places paths in x,y,x,y,x,y format requiring twice space


		//c. Now copy the path information over to the databank. Since we are
		//	working backwards from the target to the start location, we copy
		//	the information to the data bank in reverse order. The result is
		//	a properly ordered set of path data, from the first step to the
		//	last.
		pathX = targetX; pathY = targetY;
		cellPosition = pathLength[pathfinderID] * 2;//start at the end	
		do
		{
			cellPosition = cellPosition - 2;//work backwards 2 integers
			pathBank[pathfinderID][cellPosition] = pathX;
			pathBank[pathfinderID][cellPosition + 1] = pathY;

			//d.Look up the parent of the current cell.	
			//tempx = parentX[pathX][pathY];
			//pathY = parentY[pathX][pathY];

			tempx = parentX[indexTranslate(pathX, pathY)];
			pathY = parentY[indexTranslate(pathX, pathY)];

			pathX = tempx;

			//e.If we have reached the starting square, exit the loop.	
		} while (pathX != startX || pathY != startY);

		//11.Read the first path step into xPath/yPath arrays
		ReadPath(pathfinderID, startingX, startingY, 1);

	}
	return path;


	//13.If there is no path to the selected target, set the pathfinder's
	//	xPath and yPath equal to its current location and return that the
	//	path is nonexistent.
noPath:
	xPath[pathfinderID] = startingX;
	yPath[pathfinderID] = startingY;
	return nonexistent;
}




//==========================================================
//READ PATH DATA: These functions read the path data and convert
//it to screen pixel coordinates.
void Heightmap::ReadPath(int pathfinderID, int currentX, int currentY,
	int pixelsPerFrame)
{
	/*
	;	Note on PixelsPerFrame: The need for this parameter probably isn't
	;	that obvious, so a little explanation is in order. This
	;	parameter is used to determine if the pathfinder has gotten close
	;	enough to the center of a given path square to warrant looking up
	;	the next step on the path.
	;
	;	This is needed because the speed of certain sprites can
	;	make reaching the exact center of a path square impossible.
	;	In Demo #2, the chaser has a velocity of 3 pixels per frame. Our
	;	tile size is 50 pixels, so the center of a tile will be at location
	;	25, 75, 125, etc. Some of these are not evenly divisible by 3, so
	;	our pathfinder has to know how close is close enough to the center.
	;	It calculates this by seeing if the pathfinder is less than
	;	pixelsPerFrame # of pixels from the center of the square.

	;	This could conceivably cause problems if you have a *really* fast
	;	sprite and/or really small tiles, in which case you may need to
	;	adjust the formula a bit. But this should almost never be a problem
	;	for games with standard sized tiles and normal speeds. Our smiley
	;	in Demo #4 moves at a pretty fast clip and it isn't even close
	;	to being a problem.
	*/

	int ID = pathfinderID; //redundant, but makes the following easier to read

	//If a path has been found for the pathfinder	...
	if (pathStatus[ID] == found)
	{

		//If path finder is just starting a new path or has reached the 
		//center of the current path square (and the end of the path
		//hasn't been reached), look up the next path square.
		if (pathLocation[ID] < pathLength[ID])
		{
			//if just starting or if close enough to center of square
			if (pathLocation[ID] == 0 ||
				(abs(currentX - xPath[ID]) < pixelsPerFrame && abs(currentY - yPath[ID]) < pixelsPerFrame))
				pathLocation[ID] = pathLocation[ID] + 1;
		}

		//Read the path data.		
		xPath[ID] = ReadPathX(ID, pathLocation[ID]);
		yPath[ID] = ReadPathY(ID, pathLocation[ID]);

		//If the center of the last path square on the path has been 
		//reached then reset.
		if (pathLocation[ID] == pathLength[ID])
		{
			if (abs(currentX - xPath[ID]) < pixelsPerFrame
				&& abs(currentY - yPath[ID]) < pixelsPerFrame) //if close enough to center of square
				pathStatus[ID] = notStarted;
		}
	}

	//If there is no path for this pathfinder, simply stay in the current
	//location.
	else
	{
		xPath[ID] = currentX;
		yPath[ID] = currentY;
	}
}


//The following two functions read the raw path data from the pathBank.
//You can call these functions directly and skip the readPath function
//above if you want. Make sure you know what your current pathLocation
//is.

//-----------------------------------------------------------------------------
// Name: ReadPathX
// Desc: Reads the x coordinate of the next path step
//-----------------------------------------------------------------------------
int Heightmap::ReadPathX(int pathfinderID, int pathLocation)
{
	int x;
	if (pathLocation <= pathLength[pathfinderID])
	{

		//Read coordinate from bank
		x = pathBank[pathfinderID][pathLocation * 2 - 2];

		//Adjust the coordinates so they align with the center
		//of the path square (optional). This assumes that you are using
		//sprites that are centered -- i.e., with the midHandle command.
		//Otherwise you will want to adjust this.
		//x = tileSize*x + .5*tileSize;

	}
	return x;
}


//-----------------------------------------------------------------------------
// Name: ReadPathY
// Desc: Reads the y coordinate of the next path step
//-----------------------------------------------------------------------------
int Heightmap::ReadPathY(int pathfinderID, int pathLocation)
{
	int y;
	if (pathLocation <= pathLength[pathfinderID])
	{

		//Read coordinate from bank
		y = pathBank[pathfinderID][pathLocation * 2 - 1];

		//Adjust the coordinates so they align with the center
		//of the path square (optional). This assumes that you are using
		//sprites that are centered -- i.e., with the midHandle command.
		//Otherwise you will want to adjust this.
		//y = tileSize*y + .5*tileSize;

	}
	return y;
}

int Heightmap::calcSteepness(int parentXval, int parentYval, int a, int b)
{
	////int steepnessCost = abs(mapData[(parentXval)+((parentYval)* (mapHeight))].heightAverageVal - mapData[(a)+((b)* (mapHeight))].heightAverageVal);
	//int steepnessCost = abs(mapData[(parentXval)+((parentYval)* (mapHeight))].heightAverageVal - mapData[(a)+((b)* (mapHeight))].heightAverageVal);
	////steepnessCost *= 5;
	//if (steepnessCost > 0)
	//{
	//	//steepnessCost *= 2;
	//}
	//
	//if (steepnessCost > 0 && steepnessCost <= 5)
	//{
	//	steepnessCost *= 10;
	//}
	//else if (steepnessCost > 5 && steepnessCost <= 10)
	//{
	//	steepnessCost *= 100;
	//}
	//else if (steepnessCost > 10 && steepnessCost <= 15)
	//{
	//	steepnessCost *= 400;
	//}
	//else
	//{
	//	steepnessCost *= 1000;
	//}
	//return steepnessCost;

	//int steepnessCost = abs(mapData[(parentXval)+((parentYval)* (mapHeight))].heightAverageVal - mapData[(a)+((b)* (mapHeight))].heightAverageVal);
	int steepnessCost = abs(mapData[(parentXval)+((parentYval)* (mapHeight))].heightAverageVal - mapData[(a)+((b)* (mapHeight))].heightAverageVal);
	//steepnessCost *= 5;
	if (steepnessCost > 0)
	{
		//steepnessCost *= 2;
	}

	if (steepnessCost > 0 && steepnessCost <= 5)
	{
		steepnessCost *= 10;
	}
	else if (steepnessCost > 5 && steepnessCost <= 10)
	{
		steepnessCost *= 100;
	}
	else if (steepnessCost > 10 && steepnessCost <= 15)
	{
		steepnessCost *= 400;
	}
	else
	{
		steepnessCost *= 1000;
	}
	return steepnessCost;


}

int Heightmap::calcWaterInterestCost(int targetX, int targetY)
{
	return((waterInterestData[(targetX)+((targetY)* (mapWidth))] / 100));
}

int Heightmap::calcRoadInterestCost(int targetX, int targetY)
{
	return((roadInterestData[(targetX)+((targetY)* (mapWidth))] / 25));
}

int Heightmap::calcUnwalkableInterestCost(int targetX, int targetY)
{
	return((unwalkableInterestData[(targetX)+((targetY)* (mapWidth))] / 50));
}
