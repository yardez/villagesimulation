#include "Settlement.h"

Settlement::Settlement()
{
	//mtRandomGenerator.seed(1337);
	//std::uniform_real_distribution<double> tempDist(0.0, 10.0);
	//std::cout << tempDist(mtRandomGenerator) <<" 1" << "\n";
}

Settlement::Settlement(double seed, int x, int y, int size, float jitter, int sections)
{
	//double seed(32456234432);
	setSettlementSeed(seed);
	//mtRandomGenerator.seed(seed); //Stock Seed
	//mtRandomGenerator2.seed(seed);


	

	//std::random_device rd;
	//mtRandomGenerator.seed(rd());
	//mtRandomGenerator2.seed(rd());

	std::uniform_real_distribution<double> tempDist(0.0, 360.0);
	//std::uniform_real_distribution<double> tempDist(0.0, (sections+2)*2);
	std::uniform_real_distribution<double> secondaryChance(0.0, 2.0);

	//for (int i(0); i < 100; i++)
		//std::cout << secondaryChance(mtRandomGenerator) << "\n";


	secondaryStreetLocs.resize((sections + 2) * 2);


	//float angleOfMainStreet(tempDist(mtRandomGenerator));
	float angleOfMainStreet(45.0f);
	//float angleOfMainStreet(tempDist(rd));


	//generateMainRoad(x, y, size, jitter*2, sections);
	generateMainRoad(x, y, size, jitter, sections);

	int halfSecLocSize = secondaryStreetLocs.size() / 2;
	int secondaryRoads(0);

	do
	{
		for (int i(0); i < secondaryStreetLocs.size(); i++)
		{
			float test = secondaryChance(mtRandomGenerator);
			//std::cout << "CHANCE CHECK:" << test << "\n";
			if (floor(secondaryChance(mtRandomGenerator)) == 1)
			{
				//if (true)
				//{

				std::uniform_real_distribution<double> sizeRandomizer(size / 2, size);
				if (i < halfSecLocSize)
				{
					generateSecondaryRoad(i, mainStreetLocs[i].x, mainStreetLocs[i].y, sizeRandomizer(mtRandomGenerator), jitter, sections / 2);
					secondaryRoads++;
				}
				else
				{
					generateSecondaryRoad(i, mainStreetLocs[i - halfSecLocSize].x, mainStreetLocs[i - halfSecLocSize].y, -(sizeRandomizer(mtRandomGenerator)), jitter, sections / 2);
					secondaryRoads++;
				}

			}
		}
	} while (secondaryRoads < std::ceilf(sections / 2));

	//std::cout << "MINIMUM SEC ROADS: " << std::ceilf(sections / 2) << "\n";

	for (int i(0); i < mainStreetLocs.size(); i++)
	{
		mainStreetLocs[i] = rotateCoodPoint(mainStreetLocs[i].x, mainStreetLocs[i].y, angleOfMainStreet, sf::Vector2i(x, y));
		for (int j(0); j < secondaryStreetLocs[i].size(); j++)
		{
			secondaryStreetLocs[i][j] = rotateCoodPoint(secondaryStreetLocs[i][j].x, secondaryStreetLocs[i][j].y, angleOfMainStreet, sf::Vector2i(x, y));
		}
		for (int j(0); j < secondaryStreetLocs[i + halfSecLocSize].size(); j++)
		{
			secondaryStreetLocs[i + halfSecLocSize][j] = rotateCoodPoint(secondaryStreetLocs[i + halfSecLocSize][j].x, secondaryStreetLocs[i + halfSecLocSize][j].y, angleOfMainStreet, sf::Vector2i(x, y));
		}
	}

	//std::cout << "\n" << "After Rotation" << "\n";
	for (int i(0); i < mainStreetLocs.size(); i++)
	{
		//std::cout << "X Loc:" << mainStreetLocs[i].x << "|| Y Loc:" << mainStreetLocs[i].y << "\n";
	}

	for (int i(0); i < mainStreetLocs.size(); i++)
	{
		
	}
	//sf::Vector2i startPoint(mainStreetLocs[0]);
	//sf::Vector2i endPoint(mainStreetLocs[mainStreetLocs.size()-1]);
	//
	//float pointDistance = std::sqrt((endPoint.x - startPoint.x) * (endPoint.x - startPoint.x) + (endPoint.y - startPoint.y) * (endPoint.y - startPoint.y));
	//pointDistance


	//mtRandomGenerator.seed(1337);
	//std::uniform_real_distribution<double> tempDist(0.0, 10.0);
	//std::cout << tempDist(mtRandomGenerator) <<" 1" << "\n";
}

bool Settlement::generateMainRoad(int x, int y, int size, float jitter, int sections)
{
	std::uniform_real_distribution<double> tempDist(0.0, 360.0);


	float angleOfMainStreet(tempDist(mtRandomGenerator));
	

	//Resize vector to hold sections plus start and end points
	mainStreetLocs.resize(sections + 2);

	//Initalise start and end point
	mainStreetLocs[0] = sf::Vector2i(size, 0);
	mainStreetLocs[sections + 1] = sf::Vector2i(-size, 0);

	float distBetweenNodes(0.0f);

	//Find distance between start and end nodes so sections can be calculated
	distBetweenNodes = abs(mainStreetLocs[(mainStreetLocs.size() - 1)].x - mainStreetLocs[0].x); //Should return already created value
	distBetweenNodes /= sections + 1;


	//Calculate jitter to apply
	float mainRoadJitter = std::ceilf(jitter);
	std::uniform_real_distribution<double> mainRoadJitterDist(-mainRoadJitter, mainRoadJitter);


	for (int i(1); i < mainStreetLocs.size() - 1; i++)
	{
		mainStreetLocs[i] = sf::Vector2i(mainStreetLocs[i - 1].x - distBetweenNodes, mainStreetLocs[i - 1].y);
	}

	//for (int i(0); i < mainStreetLocs.size(); i++)
	//{
	//	//std::cout << mainStreetLocs[i].x << "\n";
	//}

	//std::cout << "\n" << "Before Rotation, After Jitter" << "\n";
	for (int i(0); i < mainStreetLocs.size(); i++)
	{
		//float roadJitterX = mainRoadJitterDist(mtRandomGenerator) / 2;
		float roadJitterX = mainRoadJitterDist(mtRandomGenerator);
		float roadJitterY = mainRoadJitterDist(mtRandomGenerator);

		//std::cout << "X Jitter:" << roadJitterX << "|| Y Jitter:" << roadJitterY << "\n";
		mainStreetLocs[i].x += std::roundf(roadJitterX);
		mainStreetLocs[i].y += std::roundf(roadJitterY);
		//std::cout << "X Loc:" << mainStreetLocs[i].x << "|| Y Loc:" << mainStreetLocs[i].y << "\n";
	}
	return true;
}

bool Settlement::generateSecondaryRoad(int id, int x, int y, int size, float jitter, int sections)
{
	std::uniform_real_distribution<double> tempDist(0.0, 360.0);
	//float angleOfMainStreet(tempDist(mtRandomGenerator));
	//float angleOfMainStreet(0.0f);
	//float angleOfMainStreet(90.0f);
	//float angleOfMainStreet(180.0f);
	//sf::Vector2i rotatedStartPoint(rotateCoodPoint(size, size, angleOfMainStreet, sf::Vector2i(x, y)));

	//RoadNode startPoint(rotatedStartPoint);
	//sf::Vector2i rotatedEndPoint = rotateCoodPoint(size, size, angleOfMainStreet + 180, sf::Vector2i(x, y));
	//RoadNode endPoint(rotatedEndPoint);

	//std::vector<sf::Vector2i> mainStreetLocs(sections+2);
	secondaryStreetLocs[id].resize(sections + 2);

	//Initalise start and end point
	secondaryStreetLocs[id][0] = sf::Vector2i(x,y);
	bool isNeg(false);
	if (size < 0)
		isNeg = true;
	secondaryStreetLocs[id][sections + 1] = sf::Vector2i(x, y + size);


	float distBetweenNodes(0.0f);

	distBetweenNodes = secondaryStreetLocs[id][(secondaryStreetLocs[id].size() - 1)].y - secondaryStreetLocs[id][0].y; //Should return already created value

	

	distBetweenNodes /= sections + 1;

	if (distBetweenNodes < 0)
		isNeg = true;



	float secondaryRoadJitter = std::ceilf(jitter);
	std::uniform_real_distribution<double> secondaryRoadJitterDist(-secondaryRoadJitter, secondaryRoadJitter);


	for (int i(secondaryStreetLocs[id].size() - 2); i > 0; i--)
	{
		if (distBetweenNodes > 0)
		{
			secondaryStreetLocs[id][i] = sf::Vector2i(secondaryStreetLocs[id][i + 1].x, secondaryStreetLocs[id][i + 1].y - distBetweenNodes);
		}
		else
		{
			secondaryStreetLocs[id][i] = sf::Vector2i(secondaryStreetLocs[id][i + 1].x, secondaryStreetLocs[id][i + 1].y - distBetweenNodes);
		}
	}

	//for (int i(0); i < secondaryStreetLocs[id].size(); i++)
	//{
	//	std::cout << secondaryStreetLocs[id][i].y << "\n";
	//}

	//std::cout << "\n" << "Secondary - Before Rotation, After Jitter" << "\n";
	for (int i(1); i < secondaryStreetLocs[id].size(); i++)
	{
		float roadJitterX = secondaryRoadJitterDist(mtRandomGenerator);
		float roadJitterY = secondaryRoadJitterDist(mtRandomGenerator) / 2;

		//std::cout << "X Jitter:" << roadJitterX << "|| Y Jitter:" << roadJitterY << "\n";
		secondaryStreetLocs[id][i].x += std::roundf(roadJitterX);
		secondaryStreetLocs[id][i].y += std::roundf(roadJitterY);
		//std::cout << "X Loc:" << secondaryStreetLocs[id][i].x << "|| Y Loc:" << secondaryStreetLocs[id][i].y << "\n";
	}
	return true;
}


std::vector<sf::Vector2i>& Settlement::getMainStreetLocs()
{


	return mainStreetLocs;
}

std::vector<sf::Vector2i>& Settlement::getSecondaryStreetLocs(int id)
{


	return secondaryStreetLocs[id];
}

Settlement::~Settlement()
{

}


sf::Vector2i Settlement::rotateCoodPoint(int offsetX, int offsetY, float angle, const sf::Vector2i point)
{
	angle = (angle *std::_Pi) / 180;
	float s = sin(angle);
	float c = cos(angle);

	float tempX= offsetX * c - offsetY * s;
	float tempY= offsetX * s + offsetY * c;

	sf::Vector2i newCoord;
	newCoord.x = tempX + point.x;
	newCoord.y = tempY + point.y;

	return newCoord;

	//// translate point back to origin:
	//point.x -= x;
	//point.y -= y;
	//
	//// rotate point
	//float xnew = p.x * c - p.y * s;
	//float ynew = p.x * s + p.y * c;
	//
	//// translate point back:
	//p.x = xnew + cx;
	//p.y = ynew + cy;
	//return p;
}

void Settlement::setSettlementSeed(double newSeed)
{
	settlementSeed = newSeed;

	mtRandomGenerator.seed(settlementSeed); //Stock Seed
	//mtRandomGenerator2.seed(settlementSeed);
}

double Settlement::getSettlementSeed()
{
	return settlementSeed;
}