#ifndef ROADNETWORK_H
#define ROADNETWORK_H

#include "RoadNode.h"
#include "Road.h"
#include <vector>
#include <algorithm>
#include <iostream>


//Designed to hold infomation about intersections

//Class designed to hold infomation about the procedural roadnetwork
class RoadNetwork
{
public:

	RoadNetwork();
	~RoadNetwork();

	void addRoad(sf::Vector2i start, sf::Vector2i end, std::vector<int>& pathInfo, bool doSplit = true);
	void addRoad(RoadNode* start, RoadNode* end, std::vector<int>& pathInfo);

	int findNearestNode(sf::Vector2i start);
	int findNearestNode(sf::Vector2i start, std::vector<int>& exclusionList);


	size_t splitRoad(RoadNode* start, std::vector<int>& pathInfo);



	//void addToCurrentRoad(RoadNode* start,);

	void drawDebugInfo(sf::RenderWindow& window, float scale);

	std::vector<Road>& getRoads();
	std::vector<RoadNode>& getNodes();

	/*
	void findPath(int startX, int startY, int targetX, int targetY);
	void findPath(sf::Vector2i start, sf::Vector2i target);
	*/

private:

	std::vector<RoadNode> roadNodeList;
	std::vector<Road> roadList;


	const int splitSize;
	const int splitGuard;

};

#endif