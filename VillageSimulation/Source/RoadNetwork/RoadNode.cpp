#include "RoadNode.h"


//Class used to hold infomation about the start-end point of a road

RoadNode::RoadNode()
{


}

RoadNode::RoadNode(int x, int y)
{
	setPositionOfNode(x,y);
}

RoadNode::RoadNode(sf::Vector2i loc)
{
	setPositionOfNode(loc);
}


RoadNode::~RoadNode()
{



}

void RoadNode::setPositionOfNode(int x, int y)
{
	positionOfNode.x = x;
	positionOfNode.y = y;

}


void RoadNode::setPositionOfNode(sf::Vector2i loc)
{
	positionOfNode = loc;

}

sf::Vector2i RoadNode::getPositionOfNode()
{
	return positionOfNode;

}