#include "RoadNetwork.h"


RoadNetwork::RoadNetwork() : splitSize(25),splitGuard(splitSize+10)
{

}

RoadNetwork::~RoadNetwork()
{

}

void RoadNetwork::addRoad(sf::Vector2i start, sf::Vector2i end, std::vector<int>& pathInfo, bool doSplit)
{
	sf::Clock clock; // starts the clock
	const int splitSize(25);
	const int splitGuard(splitSize + 5);

	size_t startPos;

	size_t pos2 = roadNodeList.size() - 1;
	size_t pos = roadNodeList.size() - 1;


	if (pathInfo.size() == 0)
		return;

	//Check if start is node
	bool isStartNode(false);

	if (roadNodeList.size() != 0)
	{
		for (int i(0); i < roadNodeList.size() - 1; i++)
		{
			if (start == roadNodeList[i].getPositionOfNode())
			{
				//If Destination is already node, use node later in road creation.
				isStartNode = true;
				pos = i;
				break;
			}
		}
	}

	RoadNode splitNode(start);
	RoadNode startNode(pathInfo[0], pathInfo[1]);
	

	bool hasSplit(false);

	//First node on road
	if (isStartNode == true)
	{
		startNode.setPositionOfNode(pathInfo[0], pathInfo[1]);
	}
	else
	{
		startNode.setPositionOfNode(pathInfo[0], pathInfo[1]);
		roadNodeList.push_back(startNode);
		pos = roadNodeList.size() - 1;
	}


	if (doSplit)
	{
		while (pathInfo.size() >= splitGuard * 2) //Doubled because of 1D array approch, 15 squares takes 30 slots
		{
		pos = splitRoad(&startNode, pathInfo);
		startNode = roadNodeList[pos];

		//hasSplit = true;
		//
		////startNode.setPositionOfNode(pathInfo[0], pathInfo[1]);
		////roadNodeList.push_back(startNode);
		////pos = roadNodeList.size() - 1;
		//
		//sf::Vector2i splitLoc(pathInfo[(splitSize * 2) - 2], pathInfo[(splitSize * 2) - 1]); //X and Y
		//
		////RoadNode SplitNode(splitLoc);
		//splitNode.setPositionOfNode(splitLoc);
		//roadNodeList.push_back(splitNode);
		//pos2 = roadNodeList.size() - 1;
		//std::vector<int> subPath(pathInfo.begin(), pathInfo.begin() + splitSize * 2);
		//addRoad(&roadNodeList[pos], &roadNodeList[pos2], subPath);
		//pathInfo.erase(pathInfo.begin(), pathInfo.begin() + (splitSize * 2));
		//
		////Use split location as new location
		//startNode = splitNode;
		//pos = pos2;
		}
	}

	if (hasSplit)
		startNode = splitNode;

	//Check if dest is node
	bool isDestNode(false);
	
	for (int i(0); i < roadNodeList.size(); i++)
	{
		if (end == roadNodeList[i].getPositionOfNode())
		{
			//If Destination is already node, use node later in road creation.
			isDestNode = true;
			pos2 = i;
			break;
		}
		
	}

	//Create Nodes
	

	//Push nodes into list
	//Get node positions from vector

	roadNodeList.push_back(startNode);
	pos = roadNodeList.size() - 1;
	if (!isDestNode)
	{
		RoadNode endNode(end);
		roadNodeList.push_back(endNode);
		pos2 = roadNodeList.size() - 1;
	}

	

	//Add road using node list
	addRoad(&roadNodeList[pos], &roadNodeList[pos2], pathInfo);

	//std::cout << "Time Taken to split generated road: " << clock.getElapsedTime().asSeconds() << std::endl;
}

void RoadNetwork::addRoad(RoadNode* start, RoadNode* end, std::vector<int>& pathInfo)
{
	Road newRoad(start, end, pathInfo);
	roadList.push_back(newRoad);
}

size_t RoadNetwork::splitRoad(RoadNode* start, std::vector<int>& pathInfo)
{
	RoadNode splitNode;
	sf::Vector2i splitLoc(pathInfo[(splitSize * 2) - 2], pathInfo[(splitSize * 2) - 1]); //X and Y
	
	//RoadNode SplitNode(splitLoc);
	splitNode.setPositionOfNode(splitLoc);
	roadNodeList.push_back(splitNode);


	//Index of new Node, and new trimmed path to follow
	size_t pos2 = roadNodeList.size() - 1;
	std::vector<int> subPath(pathInfo.begin(), pathInfo.begin() + splitSize * 2);


	addRoad(start, &roadNodeList[pos2], subPath);
	pathInfo.erase(pathInfo.begin(), pathInfo.begin() + (splitSize * 2));
	
	//Use split location as new location
	//startNode = splitNode;
	return pos2;
}

void RoadNetwork::drawDebugInfo(sf::RenderWindow& window,float scale)
{
	//std::vector<RoadNode> roadNodeList;
	int tempRadius(10);
	sf::CircleShape shape(tempRadius, 20);
	
	shape.setFillColor(sf::Color::Black);
	sf::Vector2i tempVec;

	for (int i(0); i < roadNodeList.size(); i++)
	{
		tempVec = roadNodeList[i].getPositionOfNode();
		shape.setPosition(tempVec.x*scale - tempRadius, tempVec.y*scale - tempRadius);
		shape.setFillColor(sf::Color::Transparent);
		shape.setOutlineThickness(10);
		shape.setOutlineColor(sf::Color(250, 150, 100));

		window.draw(shape);
	}

}

int RoadNetwork::findNearestNode(sf::Vector2i start)
{
	std::vector<int> emptyVec;
	return findNearestNode(start, emptyVec);
}

int RoadNetwork::findNearestNode(sf::Vector2i start, std::vector<int>& exclusionList)
{
	size_t closestNodeIndex;
	float closestDistance(99999999.0f);
	float currentNodeDistance(0.0f);
	sf::Vector2i currentNodeLoc;

	bool isExcluded(false);

	for (int i(0); i < roadNodeList.size(); i++)
	{
		isExcluded = false;
		for (int j(0); j < exclusionList.size(); j++)
		{
			if (exclusionList[j] == i)
			{
				isExcluded = true;
			}
			//else
			//{
			//	isExcluded = false;
			//}
		}

		if (!isExcluded)
		{
			currentNodeLoc = roadNodeList[i].getPositionOfNode();
			currentNodeDistance = (currentNodeLoc.x - start.x) * (currentNodeLoc.x - start.x) + (currentNodeLoc.y - start.y) * (currentNodeLoc.y - start.y);
			if (currentNodeDistance < closestDistance)
			{
				closestDistance = currentNodeDistance;
				closestNodeIndex = i;
			}
		}
	}

	return closestNodeIndex;
}



std::vector<Road>& RoadNetwork::getRoads()
{
	return roadList;
}

std::vector<RoadNode>& RoadNetwork::getNodes()
{
	return roadNodeList;
}

/*S
void RoadNetwork::findPath(int startX, int startY, int targetX, int targetY)
{						  


}

void RoadNetwork::findPath(sf::Vector2i start, sf::Vector2i target)
{						   


}
*/