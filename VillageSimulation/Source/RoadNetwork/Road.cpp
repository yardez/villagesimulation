#include "Road.h"


Road::Road(RoadNode* first, RoadNode* second, std::vector<int>& tempPath)
{
	roadNodes.first = first;
	roadNodes.second = second;
	roadPath = tempPath;
}

Road::~Road()
{


}

void Road::setRoadNodes(RoadNode* first, RoadNode* second)
{
	roadNodes.first = first;
	roadNodes.second = second;
}

void Road::setRoadNodes(std::pair<RoadNode*, RoadNode*> nodePair)
{
	setRoadNodes(nodePair.first, nodePair.second);
}

std::pair<RoadNode*, RoadNode*> Road::getRoadNodes()
{
	return roadNodes;


}


void Road::setPathInfo(std::vector<int>& tempPath)
{


}

std::vector<int>& Road::getPathInfo()
{

	return roadPath;
}

