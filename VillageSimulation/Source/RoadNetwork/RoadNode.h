#ifndef ROADNODE_H
#define ROADNODE_H

#include <SFML/Graphics.hpp>


//Class used to hold infomation about the start-end point of a road
class RoadNode
{
public:
	RoadNode();
	RoadNode(int x, int y);
	RoadNode(sf::Vector2i loc);
	~RoadNode();

	void setPositionOfNode(int x, int y);
	void setPositionOfNode(sf::Vector2i loc);
	sf::Vector2i getPositionOfNode();

private:

	sf::Vector2i positionOfNode;
};


#endif