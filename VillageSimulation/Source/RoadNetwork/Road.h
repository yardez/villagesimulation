#ifndef ROAD_H
#define ROAD_H

#include "RoadNode.h"



//Class to hold infomation for links between nodes
class Road
{
public:
	Road(RoadNode* first, RoadNode* second, std::vector<int>& tempPath);
	~Road();

	void setRoadNodes(RoadNode* first, RoadNode* second);
	void setRoadNodes(std::pair<RoadNode*, RoadNode*> nodePair);
	std::pair<RoadNode*, RoadNode*> getRoadNodes();


	void setPathInfo(std::vector<int>& tempPath);
	std::vector<int>& getPathInfo();


private:
	std::pair<RoadNode*, RoadNode*> roadNodes; // Holds start-end point of road
	std::vector<int> roadPath; //Holds pathinfo
};

#endif