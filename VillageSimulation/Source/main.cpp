#include <SFML/Graphics.hpp>
#include <SFML/Window/Event.hpp>
#include <vector>
#include <algorithm>
#include <string>
#include "Region.h"
#include <random>
#include <cstdint>
#include <future>
#include <thread>
#include <sstream>
#include <iomanip>

void addCircle(std::vector<sf::CircleShape> &listOfCircles, float radius, int smoothness, sf::Vector2i position)
{
	sf::CircleShape shape(radius, smoothness);
	shape.setPosition(position.x-radius,position.y-radius);
	shape.setFillColor(sf::Color::Black);
	listOfCircles.push_back(shape);
}

void renderVector(std::vector<sf::CircleShape> &listOfCircles, sf::RenderWindow &window)
{
	for (int i(0); i < listOfCircles.size(); i++)
	{
		window.draw(listOfCircles[i]);
	}

}

//void changeImage(sf::Texture* texturePTR, sf::Sprite* spritePTR, Heightmap* heightmapPTR)
//{
//	sf::Image* tempImage;
//	tempImage = &heightmapPTR->createHeightMapImage();
//	texturePTR->loadFromImage(*tempImage);
//
//	spritePTR->setTexture(*texturePTR, true);
//	spritePTR->setScale(2, 2);
//
//}



//void regionThread(std::vector<Region>& regionMapVector, int regionIndex, int seed, int xLoc, int yLoc)
//{
//
//	regionMapVector[regionIndex].loadHeightMap("heightmapData/simple1.bmp");
//	regionMapVector[regionIndex].setRegionSeed(round(distSeeds(mtRandomGenerator)));
//	regionMapVector[regionIndex].setRegionLocation(j - regionMapWidth, i - regionMapHeight);
//	regionMapVector[regionIndex].generateInterestMaps();
//	regionMapVector[regionIndex].startRegionSkeleton();
//	regionMapVector[regionIndex].drawRoads();
//	regionMapVector[regionIndex].drawRoads();
//
//
//}

int main()
{
	std::mt19937 mtRandomGenerator;
	std::srand(time(NULL));

	sf::RenderWindow window(sf::VideoMode(1024, 1024), "Village Growth Simulation - Luke Evans");
	window.setFramerateLimit(30);
	std::vector<sf::CircleShape> listOfCircles;

	bool isFocus(true);
	bool isMousePressed(true);
	double tempSeed(0.0);
	//do
	//{
	//	std::cout << "Please enter a numerical seed (3-10 digits): ";
	//	std::cin >> tempSeed;
	//	//std::cin.clear();
	//	//std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	//	//std::getline(tempSeed);
	//} while (tempSeed == 0.0);


	int seedTypeSelection(-1);
	int seedOptionSelection(-1);
	int mapTypeSelection(-1);
	int mapOptionSelection(-1);
	float heightScaleVal(1.0f);
	//seed(32456234432); // ocean drive
	std::cout << "|| Select Option ||" << "\n";
	std::cout << "1 : Manual Seed" << "\n";
	std::cout << "2 : Test Seeds" << "\n";
	do
	{
		std::cin >> seedTypeSelection;
	} while (!(seedTypeSelection > 0 && seedTypeSelection < 3));

	std::cout << "\n";

	//if (selection == 1)// Manual
	//{
	switch (seedTypeSelection)
	{
	case 1: //Test
		do
		{
			std::cout << "Please enter a numerical seed (3-10 digits): ";
			std::cin >> tempSeed;
			//std::cin.clear();
			//std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			//std::getline(tempSeed);
		} while (tempSeed == 0.0);
		break;
		//}
	case 2: //Test
		std::cout << "\n" << "\n";
		std::cout << "1 : Example Seed 1" << "\n";
		std::cout << "2 : Example Seed 2" << "\n";
		std::cout << "3 : Example Seed 3" << "\n";
		std::cout << "4 : Example Seed 4" << "\n";
		do
		{
			std::cout << "Please enter your selection: ";
			std::cin >> seedOptionSelection;
			//std::cin.clear();
			//std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			//std::getline(tempSeed);
		} while (!(seedOptionSelection > 0 && seedOptionSelection < 5));

		switch (seedOptionSelection)
		{
		case 1:
			tempSeed = 23478234;
			break;
		case 2:
			tempSeed = 467643;
			break;
		case 3:
			tempSeed = 532;
			break;
		case 4:
			tempSeed = 153372;

			break;
		}


		break;
	}
	std::uniform_real_distribution<double> distSeeds(0, 9999999999); 
	mtRandomGenerator.seed(tempSeed);
	std::cout << "\n" << "\n";

	
	//tempSeed = tempString.c_str();
	//tempSeed == tempString.c_str();
	//std::cout << "\n";
	//std::cout << tempSeed << "\n";
	//Region regionData(tempSeed);
	//Region regionData2(tempSeed+5);


	std::vector<Region> regionMapVector;
	int regionMapWidth(3);
	int regionMapHeight(3);
	int regionMapVectorSize(0);
	std::string resourcePath;

	std::cout << "|| Select Option ||" << "\n";
	std::cout << "1 : Single Map" << "\n";
	std::cout << "2 : TESTING - 3x3 Region" << "\n";

	do
	{
		std::cin >> mapTypeSelection;
	} while (!(mapTypeSelection > 0 && mapTypeSelection < 3));

	std::cout << "\n";
	switch (mapTypeSelection)
	{
	case 1: //Test
		regionMapVectorSize = 1;
		regionMapHeight = 1;
		regionMapWidth = 1;

		std::cout << "\n" << "\n";

		std::cout << "|| Select Option ||" << "\n";
		std::cout << "1 : Example Map" << "\n";
		std::cout << "2 : Custom Terrain" << "\n";

		do
		{
			std::cin >> mapOptionSelection;
		} while (!(mapOptionSelection > 0 && mapOptionSelection < 3));

		switch (mapOptionSelection)
		{
			case 1:
				resourcePath = "heightmapData/Simple1.bmp";
				heightScaleVal = 3.0f;
				break;
			case 2:
				std::string imageName;
				resourcePath = "heightmapData/customImages/";

				std::cout << "\n" << "\n";

				std::cout << "Please enter image name (\"sample.png\"): ";
				std::cin >> imageName;
				resourcePath = resourcePath + imageName;
				heightScaleVal = 3.0f;
				break;

		}


		break;
	case 2: //Test
		regionMapVectorSize = regionMapWidth * regionMapHeight;
		resourcePath = "heightmapData/regionGrid/images/regionMap_";
		break;
	}


	regionMapVector.resize(regionMapVectorSize);
	
	int regionIndex;

	for (int i(0); i < regionMapHeight; i++)
	{
		sf::Clock clock; // starts the clock
		for (int j(0); j < regionMapWidth; j++)
		{
			
			regionIndex = ((i*regionMapWidth) + j);
			regionMapVector[regionIndex].getHeightmap()->setScaleVal(heightScaleVal);
			std::stringstream ss;
			if (mapTypeSelection == 2)
			{
				ss << std::setw(2) << std::setfill('0') << (((i*regionMapWidth) + j) + 1);
			

				if (!regionMapVector[regionIndex].loadHeightMap(resourcePath + ss.str() + ".png"))
				{
					std::cout << "\n" << (resourcePath + ss.str() + ".png");
					std::cout << "\n" << "File Not Found!";
					std::cout << "\n" << "The Application will now terminate." << "\n";
					system("pause");
					
					
					return 0;
				}
				std::cout << (resourcePath + ss.str() + ".png");
				regionMapVector[regionIndex].setRegionLocation(j - regionMapWidth, i - regionMapHeight);

			}
			else
			{
				if (!regionMapVector[regionIndex].loadHeightMap(resourcePath))
				{
					std::cout << "\n" << (resourcePath);
					std::cout << "\n" << "File Not Found!";
					std::cout << "\n" << "The Application will now terminate." << "\n";
					system("pause");
					return 0;
				}

			
				//std::cout << "resourcePath";
				regionMapVector[regionIndex].setRegionLocation(0,0);
			}
			
			regionMapVector[regionIndex].setRegionSeed(round(distSeeds(mtRandomGenerator)));
			
			regionMapVector[regionIndex].generateInterestMaps();
			regionMapVector[regionIndex].startRegionSkeleton();
			regionMapVector[regionIndex].drawRoads();
			regionMapVector[regionIndex].getHeightmap()->displayRoadInterestTime();			
		}
		std::cout << "Time Taken To Generate Region : " << clock.getElapsedTime().asSeconds() << std::endl;
		std::cout << "\n";
		std::cout << "Seed used : " << tempSeed;
	}

	sf::View currentView(sf::FloatRect(0, 0, 1024, 1024));
	float zoomFactor = 1.0f;


	int mouseWheelScrolled = 0;

	bool mousePanning = false;
	bool pathDrawing = false;

	bool debugInfo(false);

	sf::Vector2i pathStart;
	sf::Vector2i pathEnd;

	sf::Vector2i closestNodeStart;

	sf::Vector2f viewPanStart;
	sf::Vector2i mousePanStart;
	sf::Vector2i mousePanCurrent;


	//Display Loop
	while (window.isOpen())
	{
		mouseWheelScrolled = 0;
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			if (event.type == sf::Event::LostFocus)
				isFocus = false;

			if (event.type == sf::Event::GainedFocus)
				isFocus = true;
			if (isFocus)
			{
				if (event.type == sf::Event::MouseWheelScrolled)
					mouseWheelScrolled = event.mouseWheelScroll.delta;

				if (event.type == sf::Event::MouseButtonPressed)
				{
					if (event.mouseButton.button == sf::Mouse::Left)
					{
						mousePanStart = sf::Mouse::getPosition(window);
						viewPanStart = currentView.getCenter();
						mousePanning = true;
					}
				}

				if (event.type == sf::Event::MouseButtonReleased)
				{
					if (event.mouseButton.button == sf::Mouse::Left)
					{
						mousePanning = false;
					}
				}

				if (event.type == sf::Event::MouseButtonPressed)
				{
					if (event.mouseButton.button == sf::Mouse::Right)
					{
						if (mapTypeSelection == 1)
						{
							if (pathDrawing == false)
							{

								pathStart = static_cast<sf::Vector2i>(window.mapPixelToCoords(sf::Mouse::getPosition(window))) / 2;
								pathStart.x = floor(pathStart.x);
								pathStart.y = floor(pathStart.y);
								std::cout << "X Loc:" << pathStart.x << "|| Y Loc:" << pathStart.y << "\n";
								pathDrawing = true;
							}
							else
							{
								pathEnd = static_cast<sf::Vector2i>(window.mapPixelToCoords(sf::Mouse::getPosition(window))) / 2;
								pathEnd.x = floor(pathEnd.x);
								pathEnd.y = floor(pathEnd.y);
								pathDrawing = false;

								regionMapVector[0].getRoadNetwork()->addRoad(sf::Vector2i(pathStart.x, pathStart.y), sf::Vector2i(pathEnd.x, pathEnd.y), regionMapVector[0].getHeightmap()->findPath(pathStart.x, pathStart.y, pathEnd.x, pathEnd.y));
								regionMapVector[0].drawRoads();
							}
						}
						//addCircle(listOfCircles, 10, 20, sf::Mouse::getPosition(window));
					}
				}

				if (event.type == sf::Event::KeyPressed)
				{
					if (event.key.code == sf::Keyboard::D)
					{
						if (debugInfo)
							debugInfo = false;
						else
							debugInfo = true;
					}

					if (event.key.code == sf::Keyboard::B)
					{
						if (mapTypeSelection == 1)
						{
							closestNodeStart = static_cast<sf::Vector2i>(window.mapPixelToCoords(sf::Mouse::getPosition(window))) / 2;
							closestNodeStart.x = floor(closestNodeStart.x);
							closestNodeStart.y = floor(closestNodeStart.y);

							regionMapVector[0].addPathFromLocation(closestNodeStart);
							regionMapVector[0].drawRoads();
						}
					}

					if (event.key.code == sf::Keyboard::T)
					{
					
						closestNodeStart = static_cast<sf::Vector2i>(window.mapPixelToCoords(sf::Mouse::getPosition(window))) / 2;
						closestNodeStart.x = floor(closestNodeStart.x);
						closestNodeStart.y = floor(closestNodeStart.y);
						//regionData.seedVillages(closestNodeStart);
						//regionData.getHeightmap()->startFillArea();
						//regionData.getHeightmap()->getEdgeVectors();
						//regionData.getHeightmap()->testEdges();
						//regionData.generateRoadConnections();
						//regionData.drawRoads();
					}
					//if (event.key.code == sf::Keyboard::G)
					//{
					//	regionData.generateSettlements();
					//}
					

				}


			}
				

			//if (isFocus)
			//{
			//	//if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left)
			//	//{
			//	//	//addCircle(listOfCircles, 10, 20, sf::Mouse::getPosition(window));
			//	//	addCircle(listOfCircles, 10, 20, sf::Vector2i(event.mouseButton.x, event.mouseButton.y));
			//	//}
			//
			//	//sf::Event::MouseButtonPressed
			//		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			//		{
			//		addCircle(listOfCircles, 10, 20, sf::Mouse::getPosition(window));
			//		}
			//
			//
			//		//Camera Movements
			//
			//		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			//		{
			//			currentView.move(0.0f, 2.0f);
			//		}
			//		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			//		{
			//
			//			currentView.move(0.0f, -2.0f);
			//		}
			//
			//		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			//		{
			//				currentView.move(-2.0f, 0.0f);
			//		}
			//		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			//		{
			//				currentView.move(2.0f, 0.0f);
			//		}
			//
			//		//Zoom Controls
			//		if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
			//		{
			//			currentView.zoom(1.01f);
			//		}
			//
			//		if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
			//		{
			//			currentView.zoom(0.99f);
			//		}
			//
			//
			//
			//
			//
			//}
		}
		if (isFocus)
		{
			//if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
			//{
			//	if (pathDrawing == false)
			//	{
			//		
			//		pathStart = sf::Mouse::getPosition(window);
			//		pathDrawing = true;
			//	}
			//	else
			//	{
			//		pathEnd = sf::Mouse::getPosition(window);
			//		pathDrawing = false;
			//	}
			//	//addCircle(listOfCircles, 10, 20, sf::Mouse::getPosition(window));
			//}
				//sf::Event::MouseWheelEvent::delta
			if (mousePanning)
			{
				mousePanCurrent = sf::Mouse::getPosition(window);
				mousePanCurrent = mousePanCurrent - mousePanStart;
				sf::Vector2f tempVec(mousePanCurrent.x, mousePanCurrent.y);
				currentView.setCenter(viewPanStart - tempVec);
			}
			

			//Camera Movements


			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				currentView.move(0.0f, 2.0f);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{

				currentView.move(0.0f, -2.0f);
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				currentView.move(-2.0f, 0.0f);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				currentView.move(2.0f, 0.0f);
			}

			//Zoom Controls
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
			{
				currentView.zoom(1.01f);
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
			{
				currentView.zoom(0.99f);
			}

			if (mouseWheelScrolled > 0)
			{
				for (int i(0); i < mouseWheelScrolled; i++)
				{
					currentView.zoom(0.90f);
				}
			}
			if (mouseWheelScrolled < 0)
			{
				for (int i(0); i < -mouseWheelScrolled; i++)
				{
					currentView.zoom(1.10f);
				}
			}
			//if (sf::Keyboard::isKeyPressed(sf::Keyboard::B))
			//{
			//	closestNodeStart = static_cast<sf::Vector2i>(window.mapPixelToCoords(sf::Mouse::getPosition(window))) / 2;
			//	closestNodeStart.x = floor(closestNodeStart.x);
			//	closestNodeStart.y = floor(closestNodeStart.y);
			//
			//	regionData.addPathFromLocation(closestNodeStart);
			//	regionData.drawRoads();
			//}
				


		}


		//if (regionData.getHeightmap()->getMapChanged())
		//	changeImage(&texture, &sprite, regionData.getHeightmap());


		//sf::Mouse::setPosition(sf::Vector2i(10, 50), window);
		window.setView(currentView);
		window.clear(sf::Color::Green);
		renderVector(listOfCircles, window);

		for (int i(0); i < regionMapHeight; i++)
		{
			for (int j(0); j < regionMapWidth; j++)
			{
				int regionIndex((i*regionMapWidth) + j);
				regionMapVector[regionIndex].drawRegionMap(window);
				if (debugInfo)
				{
					regionMapVector[regionIndex].drawDebugInfo(window, 2.0);
					regionMapVector[regionIndex].drawWaterInterestMap(window);
					regionMapVector[regionIndex].drawRoadInterestMap(window);
					regionMapVector[regionIndex].drawEdgeConnectMap(window);
					regionMapVector[regionIndex].drawUnwalkableInterestMap(window);
				}
			}
		}
		//regionData.drawRegionMap(window);
		//regionData2.drawRegionMap(window);

		if (debugInfo)
		{
			//regionData.drawDebugInfo(window, 2.0);
			//regionData.drawWaterInterestMap(window);
			//regionData.drawRoadInterestMap(window);
			//regionData.drawEdgeConnectMap(window);
			//regionData.drawUnwalkableInterestMap(window);
			//
			//regionData2.drawDebugInfo(window, 2.0);
			//regionData2.drawWaterInterestMap(window);
			//regionData2.drawRoadInterestMap(window);
			//regionData2.drawEdgeConnectMap(window);
			//regionData2.drawUnwalkableInterestMap(window);
		}

		


		//window.draw(sprite);
		//window.draw(shape);
		window.display();
	}

	return 0;
}