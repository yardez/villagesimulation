#ifndef REGION_H
#define REGION_H

#include "Map/Heightmap.h"
#include "RoadNetwork/RoadNetwork.h"
#include "Map/Settlement.h"

#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <math.h>

#include <random>
#include <cstdint>

#include <SFML/Graphics.hpp>
#include <SFML/Window/Event.hpp>

struct buildingSeedInfo
{
	int x = 0;
	int y = 0;
	int angle = 0;
};


//Class to hold infomation for Region
class Region
{

public:
	Region();
	Region(double seed);
	~Region();

	bool loadHeightMap(const std::string imagePath);
	void setRegionLocation(int xLoc, int yLoc);

	Heightmap* getHeightmap();
	RoadNetwork* getRoadNetwork();
	void addRandomPath();
	int addPathFromLocation(sf::Vector2i startLoc, bool makePath = true, int* = NULL);
	void drawRoads();
	void getValidStartPoints();
	void startRegionSkeleton();

	void seedVillages();
	void seedVillages(sf::Vector2i potentialLocation);

	void drawRegionMap(sf::RenderWindow& window);
	void drawDebugInfo(sf::RenderWindow& window, float scale);
	void drawWaterInterestMap(sf::RenderWindow& window);
	void drawRoadInterestMap(sf::RenderWindow& window);
	void drawEdgeConnectMap(sf::RenderWindow& window);
	void drawUnwalkableInterestMap(sf::RenderWindow& window);

	void generateRoadConnections();


	void generateSettlements();
	void generateSettlementGeometry(int settlementIndex);
	std::vector<buildingSeedInfo> generateSettlementGeometryRoads(int settlementIndex);
	void generateSettlementGeometryBuildingsSeeds(std::vector<int>& tempPathInfo, std::vector<buildingSeedInfo>& buildingSeeds, int min = 1, int max = 10);
	void generateSettlementGeometryBuildings(std::vector<buildingSeedInfo> buildingSeeds);
	void connectSettlementToNetwork(int settlementIndex, int startingNodes);

	void Region::generateEdgeConnections();

	void setRegionSeed(double newSeed);
	double getRegionSeed();

	//void generateBuildingSeeds();

	void generateInterestMaps();

	
	

	

private:
	Heightmap regionHeightmap;
	RoadNetwork regionRoadNetwork;

	std::mt19937 mtRandomGenerator;
	double regionSeed;

	std::vector<Settlement*> regionSettlements;

	std::vector <sf::Vector2i> regionSettlementSeeds;

	void cleanRoadData();
	void changeImage(sf::Texture* texturePTR, sf::Sprite* spritePTR, sf::Image* imagePTR);

	std::vector<int> findPath(int startX, int startY, int targetX, int targetY);
	std::vector<int> findPath(sf::Vector2i Start, sf::Vector2i End);
	bool findPath(RoadNode* Start, RoadNode* End);

	sf::Texture regionTexture;
	sf::Sprite regionSprite;

	sf::Texture waterInterestTexture;
	sf::Sprite waterInterestSprite;

	sf::Texture roadInterestTexture;
	sf::Sprite roadInterestSprite;

	sf::Texture edgeConnectTexture;
	sf::Sprite edgeConnectSprite;

	sf::Texture unwalkableInterestTexture;
	sf::Sprite unwalkableInterestSprite;

	double regionTime;

	sf::Vector2f spriteLocation;


	//sf::Vector2i 


};

#endif