#include "Region.h"



Region::Region()
{
	//generateSettlements();
	//double seed(32456234432); // ocean drive
	//double seed(83433454432);
	//double seed(6721323312);
	//double seed(7983434432);
	//double seed(1963432332);
	//double seed(7435433);
	//double seed(447435433);

	setRegionSeed(0);
	regionHeightmap.setSeed(0);

}

Region::Region(double seed)
{
	//generateSettlements();
	//double seed(32456234432); // ocean drive
	//double seed(83433454432);
	//double seed(6721323312);
	//double seed(7983434432);
	//double seed(1963432332);
	//double seed(7435433);
	//double seed(447435433);

	setRegionSeed(seed);
	regionHeightmap.setSeed(seed);

}


Region::~Region()
{

}

void Region::setRegionLocation(int xLoc, int yLoc)
{
	
	spriteLocation.x = ((regionHeightmap.getCols()-1) * xLoc) * 2; //Scale Location
	spriteLocation.y = ((regionHeightmap.getRows()-1) * yLoc) * 2;
	
	regionSprite.setPosition(spriteLocation);
}


bool Region::loadHeightMap(const std::string imagePath)
{
	
	return regionHeightmap.loadHeightMap(imagePath);
	
}

Heightmap* Region::getHeightmap()
{

	return &regionHeightmap;
}

RoadNetwork* Region::getRoadNetwork()
{

	return &regionRoadNetwork;
}

std::vector<int>  Region::findPath(int startX, int startY, int targetX, int targetY)
{
	return regionHeightmap.findPath(startX, startY, targetX, targetY);
}

std::vector<int> Region::findPath(sf::Vector2i start, sf::Vector2i end)
{

	return findPath(start.x, start.y, end.x, end.y);

}

void Region::generateRoadConnections()
{
	std::vector<sf::Vector2i>* tempRoadPoints = regionHeightmap.getRoadAccessPoints();

	sf::Vector2i start;
	sf::Vector2i end;

	if (tempRoadPoints->size() > 1)//Should always be greater than 1
	{
		start = tempRoadPoints->at(0);
		end = tempRoadPoints->at(1);


		std::vector<int> tempVec = findPath(start.x,start.y,end.x,end.y);
		if (tempVec.size() > 0) //should always be above due to previous computation
		{
			regionRoadNetwork.addRoad(sf::Vector2i(start.x, start.y), sf::Vector2i(end.x, end.y), tempVec);
			regionHeightmap.setRoadInfo(tempVec);
		}
	}

	for (int i(2); i < tempRoadPoints->size(); i++)
	{
		start = tempRoadPoints->at(i);
		//end = tempRoadPoints->at(i+1);

		addPathFromLocation(start);
	}


	//regionRoadNetwork.addRoad(sf::Vector2i(pathStart.x, pathStart.y), sf::Vector2i(pathEnd.x, pathEnd.y), regionData.getHeightmap()->findPath(pathStart.x, pathStart.y, pathEnd.x, pathEnd.y));


	//sf::Vector2i start = edgeConnections[i][j][edgeConnections[i][j].size() / 2]; //use mid point of edge for path test
	//sf::Vector2i end = edgeConnections[i][k][edgeConnections[i][k].size() / 2];
	//std::vector<int> tempVec = findPath(start.x, start.y, end.x, end.y);



}

void Region::addRandomPath()
{
	const int randSize(1024);
	bool checkFlag(false);
	std::vector<int> tempVec;
	do
	{ 

		tempVec = findPath(rand() % randSize, rand() % randSize, rand() % randSize, rand() % randSize);
		if (tempVec.size() != 0)
		{
			regionRoadNetwork.addRoad(sf::Vector2i(tempVec[0], tempVec[1]), sf::Vector2i(tempVec[tempVec.size() - 2], tempVec[tempVec.size() - 1]), tempVec);
			checkFlag = true;
		}
	} while (!checkFlag);

}

void Region::getValidStartPoints()
{
	std::uniform_real_distribution<double> edgeChance(0.0, 5.0);

	int rowSize = regionHeightmap.getRows();
	int colSize = regionHeightmap.getCols();

	std::uniform_real_distribution<double> tempRowDist(0.0, rowSize);
	std::uniform_real_distribution<double> tempColDist(0.0, colSize);

	std::vector<sf::Vector2i> edgeWalkability = regionHeightmap.getEdgeWalkability();

}
void Region::startRegionSkeleton()
{
	//Check edges for valid connections
	sf::Clock clock; // starts the clock
	regionHeightmap.testEdges();
	std::cout <<"\nTime Taken To Test Edges : " << clock.getElapsedTime().asSeconds() << std::endl;

	//Generate roads between connections

	clock.restart();
	generateRoadConnections();
	std::cout << "\nTime Taken To Generate Road Connections : " << clock.getElapsedTime().asSeconds() << std::endl;

	drawRoads();
	//Seed Villages(TEMP)

	clock.restart();
	seedVillages();
	std::cout << "\nTime Taken To Seed Village Location : " << clock.getElapsedTime().asSeconds() << std::endl;


	//std::cout << "\nTest";

	clock.restart();
	generateSettlements();
	std::cout << "\nTotal Time Taken To generate Settlement : " << clock.getElapsedTime().asSeconds() << std::endl;



}

void Region::seedVillages()
{
	int radius(50);
	sf::Vector2i potentialLocation;
	//Temp Testing Code
	std::vector<tileData>* mapData = regionHeightmap.getMapData();
	//std::vector<int> sortArray(4, 0);
	int attempts(500);

	int col = (regionHeightmap.getCols() - 2) - radius; //Temp
	int row = (regionHeightmap.getRows() - 2) - radius;
	std::uniform_real_distribution<double> distVillageCol(radius, col);
	std::uniform_real_distribution<double> distVillageRow(radius, row);

	bool locationCheck(false);
	do
	{
		potentialLocation.x = round(distVillageCol(mtRandomGenerator));
		potentialLocation.y = round(distVillageRow(mtRandomGenerator));
		locationCheck = regionHeightmap.checkVillagePlacement(potentialLocation, radius);
		//if (locationCheck == true)
		//{
		//	std::cout << "Area Clear \n";
		//}
		//else if (locationCheck == false)
		//{
		//	std::cout << "Area Blocked \n";
		//}
	} while (locationCheck == false);

	//for (int i(0); i < attempts; i++)
	//{
	//	potentialLocation.x = round(distVillageCol(mtRandomGenerator));
	//	potentialLocation.y = round(distVillageRow(mtRandomGenerator));
	//	temp = regionHeightmap.checkVillagePlacement(potentialLocation, radius);
	//	if (temp == true)
	//	{
	//		std::cout << "Area Clear \n";
	//		break;
	//	}
	//	else if (temp ==false)
	//	{
	//		std::cout << "Area Blocked \n";
	//	}
	//}

	if (locationCheck == true)
	{
		regionSettlementSeeds.push_back(sf::Vector2i(potentialLocation.x, potentialLocation.y));
	}


	//bool temp = regionHeightmap.checkVillagePlacement(testPoint, radius);

	//if (locationCheck == true)
	//	std::cout << "Area Clear \n";
	//else
	//	std::cout << "Area Blocked \n";



}

void Region::seedVillages(sf::Vector2i potentialLocation)
{
	int radius(50);
	
	bool temp = regionHeightmap.checkVillagePlacement(potentialLocation, radius);
	//if (temp == true)
	//	std::cout << "Area Clear \n";
	//else
	//	std::cout << "Area Blocked \n";



}


void Region::generateEdgeConnections()
{




}


int Region::addPathFromLocation(sf::Vector2i startLoc, bool makePath, int* nodesToCheck)
{
	sf::Clock clock; // starts the clock
	std::vector<int> tempVec;
	std::vector<int> exclusionVec;
	std::vector <int> sortedNodes; //Holds Index

	std::vector<RoadNode> roadNodeList = regionRoadNetwork.getNodes();
	std::vector<std::pair<std::vector<int>,float>> roadNodePaths;


	//Allow for a (pathDifferenceGuard) increase from bird's eye path before trying new node
	const float pathDiffGuard(1.025f);
	//const float pathDiffGuard(1.125f);
	const float pathDiffIncrease(0.10f);
	float pathDiffGuardTotal(pathDiffGuard);
	int currentStraightCost(0);
	int lastNodeIndex(-1);
	int nodeCount(0); 
	int lowestPathCost(99999999);
	int lowestPathNodeIndex(-1);
	int pathSize(-1);

	bool pathFound(false);

	//bool notFound(true);

	//if (nodesToCheck != NULL)
	//{
	//	for (int i(0); i < nodesToCheck->size(); i++)
	//	{
	//		for (int j(0); j < roadNodeList.size(); j++)
	//		{
	//			RoadNode& tempNode = nodesToCheck->at(i);
	//			if (roadNodeList[j].  tempNode)
	//		}
	//	}
	//
	//
	//}
	if (nodesToCheck != NULL)
	{
			//exclusionVec.resize(nodesToCheck->size());
		for (int i(*nodesToCheck-1); i < roadNodeList.size(); i++)
		{
			exclusionVec.push_back(i);
		}
	}


	sortedNodes.resize(roadNodeList.size());

	if (regionRoadNetwork.getNodes().size() > 0)
	{
		for (int i(0); i < regionRoadNetwork.getNodes().size(); i++)
		{
			sortedNodes[i] = regionRoadNetwork.findNearestNode(startLoc, exclusionVec);

			//Get node location
			RoadNode& tempNode = roadNodeList[sortedNodes[i]];
			sf::Vector2i currentNodeLoc = tempNode.getPositionOfNode();
			

			currentStraightCost = std::sqrt((currentNodeLoc.x - startLoc.x) * (currentNodeLoc.x - startLoc.x) + (currentNodeLoc.y - startLoc.y) * (currentNodeLoc.y - startLoc.y));
			tempVec = findPath(startLoc, currentNodeLoc);

			// Valid path check
			if (tempVec.size() != 0)
			{
				pathSize = tempVec.size();
				if (makePath)
				{
					//Divide path size by 2  due to 1D x/y allocation and check against straight line check.
					if ((tempVec.size() / 2) > currentStraightCost*pathDiffGuardTotal)
					{
						std::pair<std::vector<int>, float> tempPair;
						tempPair.first = tempVec;
						tempPair.second = currentStraightCost;
						roadNodePaths.push_back(tempPair);

						if (tempVec.size() < lowestPathCost)
						{
							lowestPathCost = tempPair.first.size();
							lowestPathNodeIndex = sortedNodes[i];
						}
					}
					else
					{
						if (tempVec.size() <= lowestPathCost)
						{
							//notFound = false;

							//std::cout << "Path Cost: " << tempVec.size() / 2 << " Line Cost: " << currentStraightCost << "lowestPathCost: " << lowestPathCost / 2 << "\n";

							regionRoadNetwork.addRoad(startLoc, tempNode.getPositionOfNode(), tempVec);
							pathFound = true;

						}
					}
				}

			}
			else
			{
				//std::cout << "No path found on first node, location unreachable \n";
				return pathSize;
			}
			if (pathFound)
				break;
		}
		if (makePath)
		{
			if (pathFound == false)
			{
				RoadNode& tempNode = roadNodeList[lowestPathNodeIndex];
				tempVec = roadNodePaths[lowestPathNodeIndex].first;
				pathSize = tempVec.size();

				regionRoadNetwork.addRoad(startLoc, tempNode.getPositionOfNode(), tempVec);
				pathFound = true;
			}
			//std::cout << "Shortest path found, road created \n";
		}
		//else
			//std::cout << "Shortest path found, road not created \n";

		
	}
	std::cout << "Time Taken To Find Nearest Valid Node : " << clock.getElapsedTime().asSeconds() << std::endl;
	return(pathSize);
}

void Region::drawRoads()
{
	std::vector<Road>& tempRoadList = regionRoadNetwork.getRoads();

	for (int i(0); i < tempRoadList.size(); i++)
	{
		regionHeightmap.setRoadInfo(tempRoadList[i].getPathInfo());
	}

}

void Region::drawRegionMap(sf::RenderWindow& window)
{
	if(regionHeightmap.getMapChanged())
		changeImage(&regionTexture, &regionSprite, &regionHeightmap.createHeightMapImage());
	window.draw(regionSprite);

}

void Region::drawDebugInfo(sf::RenderWindow& window, float scale)
{

	regionRoadNetwork.drawDebugInfo(window, scale);
}


void Region::drawWaterInterestMap(sf::RenderWindow& window)
{
	if (regionHeightmap.getWaterInterestChanged())
		changeImage(&waterInterestTexture, &waterInterestSprite, &regionHeightmap.generateWaterInterestMap());
	window.draw(waterInterestSprite);
}

void Region::drawRoadInterestMap(sf::RenderWindow& window)
{
	if (regionHeightmap.getRoadInterestChanged())
		changeImage(&roadInterestTexture, &roadInterestSprite, &regionHeightmap.generateRoadInterestMap());
	window.draw(roadInterestSprite);
}

void Region::drawEdgeConnectMap(sf::RenderWindow& window)
{
	if (regionHeightmap.getEdgeConnectChanged())
		changeImage(&edgeConnectTexture, &edgeConnectSprite, &regionHeightmap.generateEdgeConnectMap());
	window.draw(edgeConnectSprite);
}

void Region::drawUnwalkableInterestMap(sf::RenderWindow& window)
{
	if (regionHeightmap.getUnwalkableInterestChanged())
		changeImage(&unwalkableInterestTexture, &unwalkableInterestSprite, &regionHeightmap.generateUnwalkableInterestMap());
	window.draw(unwalkableInterestSprite);
}

void Region::changeImage(sf::Texture* texturePTR, sf::Sprite* spritePTR, sf::Image* imagePTR)
{
		sf::Image* tempImage;
		tempImage = imagePTR;
		texturePTR->loadFromImage(*tempImage);

		spritePTR->setTexture(*texturePTR, true);
		spritePTR->setScale(2, 2);
}

void Region::cleanRoadData()
{
	regionHeightmap.clearRoadInfo();
}


bool Region::findPath(RoadNode* start, RoadNode* end)
{
	std::vector<int> tempRoadInfo = findPath(start->getPositionOfNode(), end->getPositionOfNode());
	if (tempRoadInfo.size() == 0)
	{
		return false;
	}
	else
	{
		regionRoadNetwork.addRoad(start, end, tempRoadInfo);

		return true;
	}


}

void Region::generateSettlements()
{
	/////int x, int y, int size, float jitter, int sections
	///Settlement* tempSettlement = new Settlement(mtRandomGenerator(), 415, 585, 50, 15, 3);
	///regionSettlements.push_back(tempSettlement);
	/////delete tempSettlement;
	///tempSettlement = new Settlement(mtRandomGenerator(), 695, 175, 50, 15, 3);
	///regionSettlements.push_back(tempSettlement);
	//regionSettlementSeeds.push_back(sf::Vector2i(415, 585));
	//regionSettlementSeeds.push_back(sf::Vector2i(695, 175));

	Settlement* tempSettlement;

	for (int i(0); i < regionSettlementSeeds.size(); i++)
	{
		//int x, int y, int size, float jitter, int sections
		tempSettlement = new Settlement(mtRandomGenerator(), regionSettlementSeeds[i].x, regionSettlementSeeds[i].y, 50, 15, 3);
		regionSettlements.push_back(tempSettlement);
		//delete tempSettlement;
	}
	for (int i(0); i < regionSettlements.size(); i++)
	{
		//std::cout << regionRoadNetwork.getNodes().size() << "\n";
		generateSettlementGeometry(i);
		//std::cout << regionRoadNetwork.getNodes().size() << "\n";
	}

}

void Region::generateSettlementGeometry(int settlementIndex)
{
	//for (int i(0); i < regionSettlements.size(); i++)
	//{
	int startingNodes = regionRoadNetwork.getNodes().size();
		std::vector<buildingSeedInfo> buildingInfo = generateSettlementGeometryRoads(settlementIndex);
		generateSettlementGeometryBuildings(buildingInfo);
		connectSettlementToNetwork(settlementIndex, startingNodes);
	//}

}

void Region::connectSettlementToNetwork(int settlementIndex, int startingNodes)
{
	int pathSize(-1);
	std::vector<sf::Vector2i>& tempMainNodesLocs = regionSettlements[settlementIndex]->getMainStreetLocs();

	//firstNode on main street
	pathSize = addPathFromLocation(tempMainNodesLocs[0], false, &startingNodes);

	//lastNode on main street
	if (pathSize > addPathFromLocation(tempMainNodesLocs[tempMainNodesLocs.size() - 1], false, &startingNodes))
	{
		//lastNode is quickest connection
		addPathFromLocation(tempMainNodesLocs[tempMainNodesLocs.size() - 1], true, &startingNodes);

	}
	else
	{
		addPathFromLocation(tempMainNodesLocs[0], true, &startingNodes);
	}
	//pathSize = addPathFromLocation(tempMainNodesLocs[tempMainNodesLocs.size()-1], false, &startingNodes);

	tempMainNodesLocs.size();



}


std::vector<buildingSeedInfo> Region::generateSettlementGeometryRoads(int settlementIndex)
{
	//int startingNodes = regionRoadNetwork.getNodes().size();
	//int endingNodes;


	const float pathDiffGuard(1.10f);
	std::vector<buildingSeedInfo> buildingSeeds;

	std::vector<sf::Vector2i>& tempMainNodesLocs = regionSettlements[settlementIndex]->getMainStreetLocs();

	//regionRoadNetwork.


	//Create main road nodes and road path
	if (tempMainNodesLocs.size() > 0)
	{
		std::vector<int> totalPathInfo;
		for (int i(0); i < tempMainNodesLocs.size() - 1; i++)
		{
			sf::Vector2i pathStart = tempMainNodesLocs[(tempMainNodesLocs.size() - i) - 2];
			sf::Vector2i pathEnd = tempMainNodesLocs[(tempMainNodesLocs.size() - i) - 1];

			std::vector<int> tempPathInfo = getHeightmap()->findPath(pathStart.x, pathStart.y, pathEnd.x, pathEnd.y);
			if (tempPathInfo.size()> 0)
			{
				regionRoadNetwork.addRoad(sf::Vector2i(pathStart.x, pathStart.y), sf::Vector2i(pathEnd.x, pathEnd.y), tempPathInfo, false);
				regionHeightmap.setRoadInfo(tempPathInfo);


				//building seeds
				if (totalPathInfo.size() != 0)
				{
					totalPathInfo.insert(totalPathInfo.end(), tempPathInfo.begin() + 2, tempPathInfo.end());
				}
				else
					totalPathInfo.insert(totalPathInfo.end(), tempPathInfo.begin(), tempPathInfo.end());

			}
			else
				i = tempMainNodesLocs.size();
			//regionRoadNetwork.addRoad(sf::Vector2i(pathStart.x, pathStart.y), sf::Vector2i(pathEnd.x, pathEnd.y), regionData.getHeightmap()->findPath(pathStart.x, pathStart.y, pathEnd.x, pathEnd.y));

			

		}
		generateSettlementGeometryBuildingsSeeds(totalPathInfo, buildingSeeds,3,6);


	}

	//Connect to region skeleton



	//Create secondary road nodes and road path
	if (tempMainNodesLocs.size() > 0)
	{
		std::vector<int> totalPathInfo;
		for (int i(0); i < tempMainNodesLocs.size() - 1; i++)
		{
			std::vector<sf::Vector2i>& tempSecondaryNodesLocs = regionSettlements[settlementIndex]->getSecondaryStreetLocs(i);
			if (tempSecondaryNodesLocs.size() > 0)
			{
				for (int j(0); j < tempSecondaryNodesLocs.size() - 1; j++)
				{
					sf::Vector2i pathStart = tempSecondaryNodesLocs[j + 1];
					sf::Vector2i pathEnd = tempSecondaryNodesLocs[j];
					std::vector<int> tempPathInfo = getHeightmap()->findPath(pathStart.x, pathStart.y, pathEnd.x, pathEnd.y);

					float currentStraightCost = std::sqrtf((pathEnd.x - pathStart.x) * (pathEnd.x - pathStart.x) + (pathEnd.y - pathStart.y) * (pathEnd.y - pathStart.y));

					if ((tempPathInfo.size()> 0) && ((tempPathInfo.size() / 2) < (currentStraightCost*pathDiffGuard)))
					{

						regionRoadNetwork.addRoad(sf::Vector2i(pathStart.x, pathStart.y), sf::Vector2i(pathEnd.x, pathEnd.y), tempPathInfo, false);
						regionHeightmap.setRoadInfo(tempPathInfo);

						//building seeds
						if (totalPathInfo.size() != 0)
						{
							totalPathInfo.insert(totalPathInfo.end(), tempPathInfo.begin() + 2, tempPathInfo.end());
						}
						else
							totalPathInfo.insert(totalPathInfo.end(), tempPathInfo.begin(), tempPathInfo.end());


					}
					else
						j = tempSecondaryNodesLocs.size();
				}
			}

			tempSecondaryNodesLocs = regionSettlements[settlementIndex]->getSecondaryStreetLocs(i + tempMainNodesLocs.size() - 1);
			if (tempSecondaryNodesLocs.size() > 0)
			{
				for (int j(0); j < tempSecondaryNodesLocs.size() - 1; j++)
				{
					sf::Vector2i pathStart = tempSecondaryNodesLocs[j + 1];
					sf::Vector2i pathEnd = tempSecondaryNodesLocs[j];
					std::vector<int> tempPathInfo = getHeightmap()->findPath(pathStart.x, pathStart.y, pathEnd.x, pathEnd.y);

					float currentStraightCost = std::sqrtf((pathEnd.x - pathStart.x) * (pathEnd.x - pathStart.x) + (pathEnd.y - pathStart.y) * (pathEnd.y - pathStart.y));

					if ((tempPathInfo.size()> 0) && ((tempPathInfo.size() / 2) < (currentStraightCost*pathDiffGuard)))
					{
						regionRoadNetwork.addRoad(sf::Vector2i(pathStart.x, pathStart.y), sf::Vector2i(pathEnd.x, pathEnd.y), tempPathInfo, false);
						regionHeightmap.setRoadInfo(tempPathInfo);

						//building seeds
						if (totalPathInfo.size() != 0)
						{
							totalPathInfo.insert(totalPathInfo.end(), tempPathInfo.begin() + 2, tempPathInfo.end());
						}
						else
							totalPathInfo.insert(totalPathInfo.end(), tempPathInfo.begin(), tempPathInfo.end());


					}
					else
						j = tempSecondaryNodesLocs.size();
				}
			}
		}

		//endingNodes = regionRoadNetwork.getNodes().size();
		generateSettlementGeometryBuildingsSeeds(totalPathInfo, buildingSeeds,7,10);

	}


	return buildingSeeds;
}


void Region::generateSettlementGeometryBuildingsSeeds(std::vector<int>& tempPathInfo, std::vector<buildingSeedInfo>& buildingSeeds, int min, int max)
{
	std::uniform_real_distribution<double> seedDist(min, max);
	int distanceBetweenFirstSeeds((int)seedDist(mtRandomGenerator) * 2);
	int distanceBetweenSecondSeeds((int)seedDist(mtRandomGenerator) * 2);


	if (tempPathInfo.size() > 0)
	{
		for (int i(0); i < tempPathInfo.size() - 2; i++)
		{
			int startX, startY, nextX, nextY;
			startX = tempPathInfo[i];
			startY = tempPathInfo[i + 1];

			nextX = tempPathInfo[(i)+2];
			nextY = tempPathInfo[(i + 1) + 2];

			int xOffset;
			int yOffset;
			float angle;

			const int offsetGap(2);

			//Road direction up
			if (startY > nextY)
			{
				//up-left
				if (startX > nextX)
				{
					xOffset = -offsetGap;
					yOffset = +offsetGap;
					angle = 225;
				}
				//up-right
				else if (startX < nextX)
				{
					xOffset = +offsetGap;
					yOffset = +offsetGap;
					angle = 135;
				}
				//up
				else
				{
					xOffset = +offsetGap;
					yOffset = 0;
					angle = 90;
				}
			}
			//Road direction down
			else if (startY < nextY)
			{
				//down-left
				if (startX > nextX)
				{
					xOffset = -offsetGap;
					yOffset = -offsetGap;
					angle = 315;
				}
				//down-right
				else if (startX < nextX)
				{
					xOffset = +offsetGap;
					yOffset = -offsetGap;
					angle = 45;
				}
				//down
				else
				{
					xOffset = +offsetGap;
					yOffset = 0;
					angle = 180;
				}
			}
			//left-right
			else
			{
				xOffset = 0;
				yOffset = offsetGap;
				angle = 90;
			}
			distanceBetweenFirstSeeds--;
			distanceBetweenSecondSeeds--;
			if (distanceBetweenFirstSeeds == 0)
			{
				buildingSeedInfo firstPos;
				firstPos.x = startX + xOffset;
				firstPos.y = startY + yOffset;
				firstPos.angle = angle;
				buildingSeeds.push_back(firstPos);
				distanceBetweenFirstSeeds = (int)seedDist(mtRandomGenerator);
			}

			if (distanceBetweenSecondSeeds == 0)
			{
				buildingSeedInfo secondPos;
				secondPos.x = startX - xOffset;
				secondPos.y = startY - yOffset;
				secondPos.angle = angle + 180;
				buildingSeeds.push_back(secondPos);
				distanceBetweenSecondSeeds = (int)seedDist(mtRandomGenerator);
			}

			//Go to next seed
			//i += (distanceBetweenSeeds * 2);
			i++;
		}
	}


}


void Region::generateSettlementGeometryBuildings(std::vector<buildingSeedInfo> buildingSeeds)
{
	//regionRoadNetwork.
	//std::vector<int> buildingTiles;
	//standard 2x2 building
	std::vector<tileData>* tempMapData = regionHeightmap.getMapData();

	for (int i(0); i < buildingSeeds.size(); i++)
	{
		int buildingAngle = buildingSeeds[i].angle;
		int x, y;
		x = buildingSeeds[i].x;
		y = buildingSeeds[i].y;
		int offsetX(0), offsetY(0);
		buildingAngle = buildingAngle % 360;
		switch (buildingAngle)
		{
		case 0:
			offsetY = -1;
			offsetX = 0;
			break;
		case 45:
			offsetY = -1;
			offsetX = 1;
			break;
		case 90:
			offsetY = 0;
			offsetX = 1;
			break;
		case 135:
			offsetY = 1;
			offsetX = 1;
			break;
		case 180:
			offsetY = 1;
			offsetX = 0;
			break;
		case 225:
			offsetY = 1;
			offsetX = -1;
			break;
		case 270:
			offsetY = 0;
			offsetX = -1;
			break;
		case 315:
			offsetY = -1;
			offsetX = -1;
			break;
		}
		std::vector<bool> spaceCheck;
		spaceCheck.resize(4);

		if (offsetX == 0)
			offsetX = 1;

		if (offsetY == 0)
			offsetY = 1;

		bool allTrue = true;
		std::vector<int> buildingTiles;

		//if (x == 410 && y == 575)
		//	allTrue = false;

		


		for (int j(0); j < 2; j++)
		{
			for (int k(0); k < 2; k++)
			{
				if (allTrue)
				{
					//if (x == 410 && y == 575)
					//{
					//	std::cout << "Chicken";
					//	std::cout << "MapDataLoc: " << (x + (k*offsetX)) + ((y + (j*offsetY))*(regionHeightmap.getCols() - 1)) << "\n";
					//}
					//if (x == 409 && y == 575)
					//{
					//	std::cout << "Turkey";
					//	std::cout << "MapDataLoc: " << (x + (k*offsetX)) + ((y + (j*offsetY))*(regionHeightmap.getCols() - 1)) << "\n";
					//}

					//x
					//if (offsetX == 0)
					//	offsetX = 1;

					//buildingTiles.push_back(x + (k*offsetX));
					//y
					//if (offsetY == 0)
					//	offsetY = 1;
					//buildingTiles.push_back(y + (j*offsetY));
					//tempMapData->at(1).
					//if (tempMapData[(x + (k*offsetX)) + ((y + (j*offsetY))*(regionHeightmap.getCols() - 1))]->hasRoad == false &&
						//tempMapData[(x + (k*offsetX)) + ((y + (j*offsetY))*(regionHeightmap.getCols() - 1))]->hasBuilding == false)
					if (tempMapData->at((x + (k*offsetX)) + ((y + (j*offsetY))*(regionHeightmap.getCols() - 1))).hasRoad == false &&
						tempMapData->at((x + (k*offsetX)) + ((y + (j*offsetY))*(regionHeightmap.getCols() - 1))).hasBuilding == false)
						spaceCheck[k + (j * 2)] = true;
					else
					{
						spaceCheck[k + (j * 2)] = false;
						allTrue = false;
					}
				}
			}
		}
		if (allTrue)
		{ 
			for (int j(0); j < 2; j++)
			{
				for (int k(0); k < 2; k++)
				{
					//if (x == 409 && y == 575)
					//{
					//	std::cout << "ChickenDinner" << "\n";
					//}
					buildingTiles.push_back(x + (k*offsetX));
					buildingTiles.push_back(y + (j*offsetY));
				}

			}
		}

		if (buildingTiles.size() > 0)
		{
			//if (x == 410 && y == 575)
			//{
			//	std::cout << "RoastChickenDinner";
			//}
			regionHeightmap.setBuildingInfo(buildingTiles);
		}
		//mapData[(tempPathInfo[i]) + ((tempPathInfo[i + 1])* (cols - 1))].hasBuilding = true;
		



	}
	//set buildings onto heightmap
	//regionHeightmap.setBuildingInfo(buildingTiles);

}

void Region::setRegionSeed(double newSeed)
{
	regionSeed = newSeed;

	mtRandomGenerator.seed(regionSeed); //Stock Seed
	regionHeightmap.setSeed(regionSeed);
	//mtRandomGenerator2.seed(settlementSeed);

}
double Region::getRegionSeed()
{
	return regionSeed;

}



void Region::generateInterestMaps()
{
	changeImage(&waterInterestTexture, &waterInterestSprite, &regionHeightmap.generateWaterInterestMap());
	changeImage(&edgeConnectTexture, &edgeConnectSprite, &regionHeightmap.generateEdgeConnectMap());
	changeImage(&unwalkableInterestTexture, &unwalkableInterestSprite, &regionHeightmap.generateUnwalkableInterestMap());
	//generateEdgeConnectMap
}